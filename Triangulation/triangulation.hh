#ifndef TRIANGULATION_HH
#define TRIANGULATION_HH

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Triangulation_data_structure_2.h>
#include <CGAL/Triangulation_vertex_base_2.h>
#include <CGAL/Triangulation_face_base_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>

#include <CGAL/Triangulation_2.h>
#include <CGAL/Delaunay_triangulation_2.h>

#include <cmath>
#include <vector>
#include <stack>

struct V_info
{
    bool marked;

    V_info()
    {
	marked = false;
    }
};

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_data_structure_2 <
    CGAL::Triangulation_vertex_base_with_info_2<V_info, K>,
    CGAL::Triangulation_face_base_2<K>> Tds;
typedef CGAL::Triangulation_2<K, Tds> Triangulation;
typedef CGAL::Delaunay_triangulation_2<K, Tds> Delaunay_triangulation;
/*
class L_1_traits : public K
{
    typedef CGAL::Comparison_result Cmp_res;
    typedef CGAL::Oriented_side Oriented_side;
    typedef CGAL::Orientation Orientation;

    typedef K Geom_traits;
    typedef K::Point_2 Point;
    typedef K::Vector_2 Vector;
    typedef K::Triangle_2 Triangle;
    typedef K::Line_2 Line;
    typedef K::Compare_x_2 Compare_x;
    typedef K::Compare_y_2 Compare_y;
    typedef K::RT Expr;

    struct Side_of_oriented_circle_2
    {
	Oriented_side operator()(const Point& p, const Point& q,
				 const Point& r, const Point& s)
	{
	    
	}
    };
};
*/
class TD_Delaunay_triangulation : public Triangulation
{
public:
    typedef K Geom_traits;
    typedef K::Point_2 Point;
    typedef K::Vector_2 Vector;
    typedef K::Triangle_2 Triangle;
    typedef K::Line_2 Line;
    typedef K::Compare_x_2 Compare_x;
    typedef K::Compare_y_2 Compare_y;
    typedef K::RT Expr;

    typedef CGAL::Comparison_result Cmp_res;
    typedef CGAL::Oriented_side Oriented_side;
    typedef CGAL::Orientation Orientation;
	
    typedef Triangulation::Face_handle Face_handle;
    typedef Triangulation::Vertex_handle Vertex_handle;
    typedef Triangulation::Edge Edge;
    typedef Triangulation::Locate_type Locate_type;

    typedef std::vector<Point> Points;
    typedef Points::const_iterator Points_cit;

    void insert(Points& pts);
    template<class Input_it> void insert(Input_it first, Input_it last);

    Vertex_handle insert(const Point& p, Face_handle start = Face_handle());

    bool in_circum_triangle(Face_handle fh, const Point& p)	const;
    bool in_circum_triangle(const Triangle& t, const Point& p)	const;

	
private:
    const Expr sqrt_3 = std::sqrt(3);
    const Vector side_h = Vector(-1, 0), side_pi_3 = Vector(.5, sqrt_3),
	side_2_pi_3 = Vector(.5, -sqrt_3);
	
    void sort_points_td(Point& a, Point& b, Point& c) const;

    Vertex_handle insert(const Point& p, Locate_type lt, Face_handle loc,
			 int li);

    void restore_Delaunay(Vertex_handle v);
    void propagating_flip(Face_handle fh, int i);
};


#endif

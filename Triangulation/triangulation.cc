#include "triangulation.hh"

void TD_Delaunay_triangulation::sort_points_td(
    Point &a, Point &b, Point &c) const
{
    Point real_a, real_b, real_c;
    
    Compare_y cmp_y;
	
    if(cmp_y(a, b) == Cmp_res::SMALLER)
    {
	if(cmp_y(b, c) == Cmp_res::SMALLER)
	    real_a = c, real_b = a, real_c = b;
	else
	    real_a = b, real_b = c, real_c = a;
    }
    else
    {
	if(cmp_y(a, c) == Cmp_res::SMALLER)
	    real_a = c, real_b = a, real_c = b;
	else
	    return;
    }

    a = real_a, b = real_b, c = real_c;
}


bool TD_Delaunay_triangulation::in_circum_triangle(
    Face_handle fh, const Point &p) const
{
    if(!is_infinite(fh))
    {	
	Point a(fh->vertex(0)->point()),
	    b(fh->vertex(1)->point()),
	    c(fh->vertex(2)->point());

	sort_points_td(a, b, c);

	Line l_h(a, side_h), l_2_pi_3(b, side_2_pi_3),
	    l_pi_3(c, side_pi_3);

	bool pos_h(l_h.has_on_positive_side(p)),
	    pos_2_pi_3(l_2_pi_3.has_on_positive_side(p)),
	    pos_pi_3(l_pi_3.has_on_positive_side(p));

	return pos_h && pos_2_pi_3 && pos_pi_3;
    }

    int i(fh->index(infinite_vertex()));
	
    Orientation o(orientation(fh->vertex(ccw(i))->point(),
			      fh->vertex(cw(i))->point(),
			      p));
        
    return o == Orientation::POSITIVE; 
}

bool TD_Delaunay_triangulation::in_circum_triangle(
    const Triangle &t, const Point &p) const
{
    Point a(t[0]),
	b(t[1]),
	c(t[2]);

    sort_points_td(a, b, c);

    Line l_h(a, side_h), l_2_pi_3(b, side_2_pi_3),
	l_pi_3(c, side_pi_3);

    bool pos_h(l_h.has_on_positive_side(p)),
	pos_2_pi_3(l_2_pi_3.has_on_positive_side(p)),
	pos_pi_3(l_pi_3.has_on_positive_side(p));

    return pos_h && pos_2_pi_3 && pos_pi_3;
}

void TD_Delaunay_triangulation::insert(Points& pts)
{
    CGAL::spatial_sort(pts.begin(), pts.end(), geom_traits());

    Face_handle fh;
    Points_cit it(pts.begin()), itend(pts.end());

    for(; it != itend; ++it)
	fh = insert(*it, fh)->face();
}

template<class Input_it>
void TD_Delaunay_triangulation::insert(Input_it first, Input_it last)
{
    Points pts(first, last);

    insert(pts);
}


Triangulation::Vertex_handle TD_Delaunay_triangulation::insert(
    const Point& p, Face_handle start)
{
    Locate_type lt;
    int li;
    Face_handle loc(this->locate(p, lt, li, start));
	
    return insert(p, lt, loc, li);
}

Triangulation::Vertex_handle TD_Delaunay_triangulation::insert(
    const Point &p, Locate_type lt, Face_handle loc, int li)
{
    std::cout << "Inserting " << p << " : " << std::endl;
    
    Vertex_handle v(Triangulation::insert(p, lt, loc, li));

    restore_Delaunay(v);
	
    return v;
}

void TD_Delaunay_triangulation::restore_Delaunay(Vertex_handle v)
{
    if(dimension() <= 1)
	return;

    Face_handle fh(v->face()), next, start(fh);
	
    int i;

    do
    {
	i = fh->index(v);
	next = fh->neighbor(ccw(i));
	propagating_flip(fh, i);
	fh = next;
    } while(next != start);

    return;
}


void TD_Delaunay_triangulation::propagating_flip(Face_handle fh, int i)
{
    Face_handle n(fh->neighbor(i));
    const Point& p(fh->vertex(i)->point()),
	opp_p(n->vertex(n->index(fh))->point());

    if(!in_circum_triangle(n, p) && !in_circum_triangle(fh, opp_p))
	return;

    this->flip(fh, i);
    propagating_flip(fh, i);
    i = n->index(fh->vertex(i));
    propagating_flip(n, i);
    
    /*
    std::stack<Edge> edges;
    const Vertex_handle& vp(fh->vertex(i));
    const Point& p(vp->point());
    edges.push(Edge(fh,i));

    while(!edges.empty())
    {
	const Edge& e(edges.top());
	fh = e.first;
	i = e.second;
		
	const Face_handle& n(fh->neighbor(i));

	if(!in_circum_triangle(n, p) &&
	   !in_circum_triangle(
	       fh, n->vertex(n->index(fh))->point()))
	{
	    edges.pop();
	    continue;
	}
	
	this->flip(fh, i);
	// Since we didn't pop it, we don't have to push it
	edges.push(Edge(n,n->index(vp)));
    }
    */
}

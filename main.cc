#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Triangulation_2.h>
#include <CGAL/Delaunay_triangulation_2.h>

#include <random>
#include <chrono>
#include <iostream>
#include <string>
#include <fstream>

#include "Triangulation/triangulation.hh"
#include "Draw/draw.hh"
#include "Hierarchy/hierarchy.hh"

#define N 1000
#define OUT(a) static_cast<double>(a.x()) << "," << static_cast<double>(a.y())

typedef Triangulation::Point Point;
typedef std::vector<Point> Points;
typedef K::FT Expr;
typedef CGAL::Real_embeddable_traits<Expr>::To_double To_double;

To_double to_double;

uint64_t init_points(Points& pts)
{
    /* Initialize the random number generator with a time-dependent
       seed. */
    std::mt19937_64 rng;
    uint64_t seed(std::chrono::high_resolution_clock::now().
		  time_since_epoch().count());

    // N = 50
    //seed = 1613476901436406563; // path 1
    //seed = 1613477107729459437; // path 2
    //seed = 1613477365439517339; // path 3
    //seed = 1613477575639110537; // path 4
    //seed = 1613491971030796279; // path 5
    //seed = 1613493423915795047; // path 6
    //seed = 1613493685685102565; // path 7
    //seed = 1613997409226503607; // Backtracks indefinitely
    //seed = 1613998719820587053;
    //seed = 1614002165786553563; // "Backtracks twice"
    //seed = 1614003352472627125;
    //seed = 1614081065046555919; // non-Delaunay edge taken
    //seed = 1614094085945496939;
    //seed = 1614168559193926604;
    //seed = 1614173868914979461;
    //seed = 1614267378719207498; // Problem with points
    //seed = 1614268338956698716;
    //seed = 1614343465894815590; // not a real path
    // N = 100
    //seed = 1614346182524216152;
    //seed = 1614598140114376654; // non-existing edge:
    // N = 1000
    //seed = 1615290517786141795;
    //seed = 1615293685646943791;
    //seed = 1615386794477850547;
    //seed = 1615390973934446557;
    //seed = 1615393566927699278;
    //seed = 1615459198161254408;
    //seed = 1615463273277063437;
    //seed = 1615466643261103310;
    //seed = 1615547319031321841; // no 17-8 edge
    //seed = 1615555460972391514;
    //seed = 1615558593946367673;
    //seed = 1615807687533347986; // lots of red
    //seed = 1615808440180517693; // proof ad_hoc doesn't work
    //seed = 1615818017657716146;
    //seed = 1615891262901194469;
    //seed = 1615892174133493366;
    //seed = 1616071792179535225;
    //seed = 1616074166602966882;
    //seed = 1616075343757145115;
    //seed = 1616079057450402783;
    //seed = 1616414638248432164;
    //seed = 1616415679523769633; // non-Delaunay edge 14-7
    //seed = 1616417094107614643;
    //seed = 1616417695917190727;
    //seed = 1616494138788196645;
    //seed = 1616596362212609235;
    //seed = 1616673420154584133;
    //seed = 1616761726501569240;
    //seed = 1616764575101347015;
    //seed = 1616767902297698520;
    //seed = 1616768973108315651; // nice one (has to go back up, messes up)
    //seed = 1617021537123566886;
    //seed = 1617106523003665328; // Seg faults
    //seed = 1617278945325610453; // Counter-example for constant fraction with risky
    //seed = 1617279598484782740; // Counter-example for 1/3 without risky
    
    std::seed_seq ss{uint32_t(seed & 0xffffffff), uint32_t(seed >> 32)};
    rng.seed(ss);

    std::ofstream log_seed("seed.txt");

    log_seed << seed << std::endl;
     
    /* Initialize a uniform distribution between 0 and 1. */
    std::uniform_real_distribution<double> unif(0, 1);
     
    for(unsigned i(0); i < N - 2; ++i)
    {
	Point p(unif(rng), unif(rng));
	  
	pts.push_back(p);
    }

    pts.push_back(Point(0, 0));
    pts.push_back(Point(1, 1));
    
    return seed;
}

void output_triangles(std::ostream& f,
		      const std::vector<Triangulation::Triangle>& trgs)
{
    typedef std::set<Triangulation::Point> Seen;

    Seen s;
    
    for(const Triangulation::Triangle& t : trgs)
    {
	if(s.find(t[0]) == s.end())
	{
	    f << OUT(t[0]) << std::endl;
	    s.insert(t[0]);
	}

	if(s.find(t[1]) == s.end())
	{
	    f << OUT(t[1]) << std::endl;
	    s.insert(t[1]);
	}

	if(s.find(t[2]) == s.end())
	{
	    f << OUT(t[2]) << std::endl;
	    s.insert(t[2]);
	}
    }

    f << std::endl;
}


int main()
{
    Points p;

    uint64_t seed(init_points(p));

    /*
    for(const Point& pt : p)
	std::cout << pt << " ; ";
    
    std::cout << std::endl;
    */
    Point s1(p[N - 2]), t1(p[N - 1]), // 0 ; N - 1
	s2(p[10]), t2(p[20]);
    
    Delaunay_triangulation dt;

    //TD_Delaunay_triangulation td_dt;

    dt.insert(p.begin(), p.end());
    //td_dt.insert(p);

    Kirkpatrick_hierarchy<Delaunay_triangulation> kh_dt(dt, 8, N);
    
    Draw d;
    
    d(dt, std::string("delaunay.svg"));
    //d(td_dt, std::string("td_delaunay.svg"));
    d(kh_dt, std::string("kirkpatrick_delaunay_"));

    std::vector<Kirkpatrick_hierarchy<Delaunay_triangulation>::Triangle> faces1,
	not_faces1, r_faces1, r_not_faces1, dummy, ad_hoc_faces1,
	ad_hoc_not_faces1, faces1_small, not_faces1_small, r_faces1_small,
	r_not_faces1_small, dummy_small, ad_hoc_faces1_small, ad_hoc_not_faces1_small;

    std::vector<
	std::vector<Kirkpatrick_hierarchy<Delaunay_triangulation>::Triangle>>
	extra_faces1, extra_faces2, extra_faces1_small, extra_faces2_small;

    kh_dt.path(s1, t1, faces1, not_faces1, extra_faces1);
    kh_dt.path_small_steps(s1, t1, faces1_small, not_faces1_small,
			   extra_faces1_small);
    
    std::cout << "1: " << std::endl;

    //kh_dt.routing_path_backtrack(s1, t1, r_faces1, r_not_faces1);
    //kh_dt.ad_hoc_routing_pov(s1, t1, ad_hoc_faces1, ad_hoc_not_faces1);
    kh_dt.ad_hoc_routing_recursive(s1, t1, ad_hoc_faces1, ad_hoc_not_faces1);
    
    d(kh_dt, "kirkpatrick_delaunay_");
    d(kh_dt, faces1, dummy, "path1.svg", s1, t1);
    d(kh_dt, faces1, not_faces1, "path1_red.svg", s1, t1);
    d(kh_dt, faces1_small, dummy, "path1_small.svg", s1, t1);
    d(kh_dt, faces1_small, not_faces1_small, "path1_small_red.svg", s1, t1);
    //d(kh_dt, r_faces1, r_not_faces1, "routing_path1.svg", s1, t1);
    //d(kh_dt, r_faces1, dummy, "routing_path1_no_red.svg", s1, t1);
    d(kh_dt, ad_hoc_faces1, ad_hoc_not_faces1, "ad_hoc_path1.svg", s1, t1);
    d(kh_dt, ad_hoc_faces1, dummy, "ad_hoc_path1_no_red.svg", s1, t1);
    
    std::ofstream f("path1.csv"), rf("routing_path1.csv"),
	af("ad_hoc_path1.csv");

    f << seed << std::endl << OUT(s1) << "," << OUT(t1) << std::endl
      << std::endl;
    rf << seed << std::endl << OUT(s1) << "," << OUT(t1) << std::endl
      << std::endl;
    af << seed << std::endl << OUT(s1) << "," << OUT(t1) << std::endl
      << std::endl;

    
    output_triangles(f, faces1);
    output_triangles(f, not_faces1);
    /*
    output_triangles(rf, r_faces1);
    output_triangles(rf, r_not_faces1);
    */
    output_triangles(af, ad_hoc_faces1);
    output_triangles(af, ad_hoc_not_faces1);
    
    std::vector<Kirkpatrick_hierarchy<Delaunay_triangulation>::Triangle> faces2,
	not_faces2, r_faces2, r_not_faces2, ad_hoc_faces2,
	ad_hoc_not_faces2;
    
    kh_dt.path(s2, t2, faces2, not_faces2, extra_faces2);

    std::cout << "2: " << std::endl;
    
    //kh_dt.routing_path_backtrack(s2, t2, r_faces2, r_not_faces2);
    //kh_dt.ad_hoc_routing_pov(s2, t2, ad_hoc_faces2, ad_hoc_not_faces2);
    kh_dt.ad_hoc_routing_recursive(s2, t2, ad_hoc_faces2, ad_hoc_not_faces2);
    
    d(kh_dt, faces2, dummy, "path2.svg", s2, t2);
    d(kh_dt, faces2, not_faces2, "path2_red.svg", s2, t2);
    //d(kh_dt, r_faces2, r_not_faces2, "routing_path2.svg", s2, t2);
    //d(kh_dt, r_faces2, dummy, "routing_path2_no_red.svg", s2, t2);
    d(kh_dt, ad_hoc_faces2, ad_hoc_not_faces2, "ad_hoc_path2.svg", s2, t2);
    d(kh_dt, ad_hoc_faces2, dummy, "ad_hoc_path2_no_red.svg", s2, t2);
    
    std::ofstream f2("path2.csv"), rf2("routing_path2.csv"),
	af2("ad_hoc_faces2.csv");

    f2 << seed << std::endl << OUT(s2) << "," << OUT(t2) << std::endl
      << std::endl;
    rf2 << seed << std::endl << OUT(s2) << "," << OUT(t2) << std::endl
      << std::endl;
    af2 << seed << std::endl << OUT(s2) << "," << OUT(t2) << std::endl
      << std::endl;
    
    output_triangles(f2, faces2);
    output_triangles(f2, not_faces2);
    /*
    output_triangles(rf2, r_faces2);
    output_triangles(rf2, r_not_faces2);
    */
    output_triangles(af2, ad_hoc_faces2);
    output_triangles(af2, ad_hoc_not_faces2);

    unsigned i(0);

    for(const auto& not_faces : extra_faces1)
    {
	d(kh_dt, faces1, not_faces, "path1_" + std::to_string(i) + "_red.svg",
	  s1, t1);

	++i;
    }

    i = 0;

    for(const auto& not_faces : extra_faces1_small)
    {
	d(kh_dt, faces1_small, not_faces, "path1_small_" + std::to_string(i) +
	  "_red.svg", s1, t1);

	++i;
    }

    i = 0;
    
    for(const auto& not_faces : extra_faces2)
    {
	d(kh_dt, faces2, not_faces, "path2_" + std::to_string(i) + "_red.svg",
	  s2, t2);

	++i;
    }
    
    return 0;
}

#include "draw.hh"

#include <iostream>

Draw::Draw()
{
}

void Draw::operator()(const Triangulation& t, const std::string filename) const
{
    All_faces_iterator it(t.all_faces_begin()),
	itend(t.all_faces_end());

    cairo_surface_t* surface;
    cairo_t* cr;

    surface = cairo_svg_surface_create(filename.c_str(), width, height);
    cr = cairo_create(surface);

    cairo_set_line_width(cr, l_width);

    cairo_set_source_rgb(cr, 0, 0, 0);

    for(; it != itend; ++it)
    {
	if(t.is_infinite(it))
	{
	    int i(it->index(t.infinite_vertex()));

	    const Point
		&a(tr_and_scale(it->vertex(t.ccw(i))->point())),
		&b(tr_and_scale(it->vertex(t.cw(i))->point()));

	    draw_segment(cr, a, b);
	    continue;
	}
		
	const Point &a(tr_and_scale(it->vertex(0)->point())),
	    &b(tr_and_scale(it->vertex(1)->point())),
	    &c(tr_and_scale(it->vertex(2)->point()));

	draw_triangle_stroke(cr, a, b, c);
    }
    
    cairo_surface_destroy(surface);
    cairo_destroy(cr);
}

void Draw::draw_triangle(cairo_t* cr, const Point& a, const Point& b,
			 const Point& c) const
{
    const Expr &e_ax(a.x()), &e_ay(a.y()),
	&e_bx(b.x()), &e_by(b.y()),
	&e_cx(c.x()), &e_cy(c.y());

    const double ax(to_double(e_ax)), ay(to_double(e_ay)),
	bx(to_double(e_bx)), by(to_double(e_by)),
	cx(to_double(e_cx)), cy(to_double(e_cy));
	
    cairo_move_to(cr, ax, ay);
    cairo_line_to(cr, bx, by);
    cairo_line_to(cr, cx, cy);
    cairo_close_path(cr);
}

void Draw::draw_triangle(cairo_t* cr, const Point& a, const Point& b,
			 const Point& c, const std::string& str_a,
			 const std::string& str_b,
			 const std::string& str_c) const
{
    const Expr &e_ax(a.x()), &e_ay(a.y()),
	&e_bx(b.x()), &e_by(b.y()),
	&e_cx(c.x()), &e_cy(c.y());

    const double ax(to_double(e_ax)), ay(to_double(e_ay)),
	bx(to_double(e_bx)), by(to_double(e_by)),
	cx(to_double(e_cx)), cy(to_double(e_cy));

    cairo_move_to(cr, ax + ux, ay + uy);
    cairo_show_text(cr, str_a.c_str());
    
    cairo_move_to(cr, ax, ay);
    cairo_line_to(cr, bx, by);
    
    cairo_move_to(cr, bx + ux, by + uy);
    cairo_show_text(cr, str_b.c_str());
    cairo_move_to(cr, bx, by);
    
    cairo_line_to(cr, cx, cy);
    
    cairo_move_to(cr, cx + ux, cy + uy);
    cairo_show_text(cr, str_c.c_str());
    cairo_move_to(cr, cx, cy);

    cairo_line_to(cr, ax, ay);
    
    cairo_close_path(cr);
}

void Draw::draw_segment(cairo_t* cr, const Point& a, const Point& b) const
{
    const Expr &e_ax(a.x()), &e_ay(a.y()),
	&e_bx(b.x()), &e_by(b.y());

    const double ax(to_double(e_ax)), ay(to_double(e_ay)),
	bx(to_double(e_bx)), by(to_double(e_by));
	
    cairo_move_to(cr, ax, ay);
    cairo_line_to(cr, bx, by);
    cairo_close_path(cr);
    cairo_stroke(cr);
}

void Draw::draw_triangle_stroke(cairo_t* cr, const Point& a, const Point& b,
				const Point& c) const
{
    draw_triangle(cr, a, b, c);
    cairo_stroke(cr);
}

void Draw::draw_triangle_stroke(cairo_t* cr, const Point& a, const Point& b,
				const Point& c, const std::string& str_a,
				const std::string& str_b,
				const std::string& str_c) const
{
    draw_triangle(cr, a, b, c, str_a, str_b, str_c);
    cairo_stroke(cr);
}

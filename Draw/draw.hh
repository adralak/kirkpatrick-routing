#ifndef DRAW_HH
#define DRAW_HH

#include <cairo/cairo.h>
#include <cairo/cairo-svg.h>

#include <istream>
#include <ostream>

#include <CGAL/Aff_transformation_2.h>
#include <CGAL/Real_embeddable_traits.h>
#include <CGAL/Triangulation_data_structure_2.h>

#include <string>

#include "../Triangulation/triangulation.hh"
#include "../Hierarchy/hierarchy.hh"

class Draw
{
public:
    typedef K::FT Expr;
    typedef K::Triangle_2 Triangle;
    typedef K::Vector_2 Vector;
    typedef K::Point_2 Point;
	
    typedef CGAL::Aff_transformation_2<K> Aff_map;
    typedef CGAL::Scaling Scaling;
    typedef CGAL::Translation Translation;

    typedef Triangulation::All_faces_iterator All_faces_iterator;
    typedef Triangulation::Face_handle Face_handle;
	
    typedef CGAL::Real_embeddable_traits<Expr>::To_double To_double;
    
    Draw();

    void operator()(const Triangulation& t, const std::string filename)
	const;

    template<class T>
    void operator()(const Kirkpatrick_hierarchy<T>& kh,
		    const std::string filename) const;

    template<class T>
    void operator()(
	const Kirkpatrick_hierarchy<T>& kh,
	const typename Kirkpatrick_hierarchy<T>::Graph& t,
	const std::string filename) const;

    template<class T>
    void operator()(Kirkpatrick_hierarchy<T>& kh,
		    const std::vector<Triangle>& faces,
		    const std::vector<Triangle>& not_faces,
		    const std::string filename,
		    const Point& s, const Point& t) const;
    
private:
    double width = 400, height = 400, l_width = 0.2,
	base_x = 100, base_y = 100, scale = 200, font_size = 6,
	ux = 0.1, uy = - 0.1;

    Vector tr = Vector(base_x, base_y);
    Aff_map tr_and_scale = Aff_map(Translation(), tr)
	* Aff_map(Scaling(), scale);
	
    const To_double to_double = To_double();

    template<class Trg_it>
    void draw_triangles(cairo_t* cr, Trg_it it, Trg_it itend, const
			Triangulation& t) const;
    void draw_triangle(cairo_t* cr, const Point& a, const Point& b,
		       const Point& c) const;
    void draw_triangle(cairo_t* cr, const Point& a, const Point& b,
		       const Point& c, const std::string& str_a,
		       const std::string& str_b,
		       const std::string& str_c) const;
    void draw_triangle_stroke(cairo_t* cr, const Point& a, const Point& b,
			      const Point& c) const;
    void draw_triangle_stroke(cairo_t* cr, const Point& a, const Point& b,
			      const Point& c, const std::string& str_a,
			      const std::string& str_b,
			      const std::string& str_c) const;

    void draw_segment(cairo_t* cr, const Point& a, const Point& b) const;
};

// Implementation below


template<class T>
void Draw::operator()(const Kirkpatrick_hierarchy<T> &kh,
		      const std::string filename) const
{
    typedef typename Kirkpatrick_hierarchy<T>::Graph Graph;

    unsigned i(0);
    
    for(const Graph& t : kh.h)
    {
	this->operator()(kh, t, filename + std::to_string(i) + ".svg");
	++i;
    }
}

template<class T>
void Draw::operator()(
    const Kirkpatrick_hierarchy<T>& kh,
    const typename Kirkpatrick_hierarchy<T>::Graph& t,
    const std::string filename) const
{
    typedef typename Kirkpatrick_hierarchy<T>::Node Node;

    cairo_surface_t* surface;
    cairo_t* cr;

    surface = cairo_svg_surface_create(filename.c_str(), width, height);
    cr = cairo_create(surface);

    cairo_set_line_width(cr, l_width);

    cairo_set_source_rgb(cr, 0, 0, 0);

    for(const Node& n : t)
    {
	const Point& a(tr_and_scale(kh.all_pts[n.p]));

	for(const unsigned m : n.neighb)
	{
	    const Point& b(tr_and_scale(kh.all_pts[m]));

	    draw_segment(cr, a, b);
	}
    }

    cairo_surface_destroy(surface);
    cairo_destroy(cr);
}

template<class T>
void Draw::operator()(
    Kirkpatrick_hierarchy<T>& kh, const std::vector<Triangle>& faces,
    const std::vector<Triangle>& not_faces,
    const std::string filename, const Point& s, const Point& t) const
{
    cairo_surface_t* surface;
    cairo_t* cr;

    surface = cairo_svg_surface_create(filename.c_str(), width, height);
    cr = cairo_create(surface);

    cairo_set_line_width(cr, l_width);
    cairo_set_source_rgb(cr, 0, 0, 255);

    cairo_select_font_face(cr, "monospace", CAIRO_FONT_SLANT_NORMAL,
			   CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size(cr, font_size);
    
    draw_segment(cr, tr_and_scale(s), tr_and_scale(t));
    
    cairo_set_source_rgb(cr, 0, 0, 0);
    
    for(const Triangle& fh : faces)
    {		
	const Point &a(tr_and_scale(fh.vertex(0))),
	    &b(tr_and_scale(fh.vertex(1))),
	    &c(tr_and_scale(fh.vertex(2)));

	const unsigned lvl_a(kh.lvls[fh.vertex(0)].lvl),
	    lvl_b(kh.lvls[fh.vertex(1)].lvl),
	    lvl_c(kh.lvls[fh.vertex(2)].lvl);

	const std::string str_a(std::to_string(lvl_a)),
	    str_b(std::to_string(lvl_b)),
	    str_c(std::to_string(lvl_c));
	
	draw_triangle_stroke(cr, a, b, c, str_a, str_b, str_c);
    }

    cairo_set_source_rgb(cr, 0, 255, 0);

    {
	const Triangle& fh(faces.back());
	
	const Point &a(tr_and_scale(fh.vertex(0))),
	    &b(tr_and_scale(fh.vertex(1))),
	    &c(tr_and_scale(fh.vertex(2)));

	const unsigned lvl_a(kh.lvls[fh.vertex(0)].lvl),
	    lvl_b(kh.lvls[fh.vertex(1)].lvl),
	    lvl_c(kh.lvls[fh.vertex(2)].lvl);

	const std::string str_a(std::to_string(lvl_a)),
	    str_b(std::to_string(lvl_b)),
	    str_c(std::to_string(lvl_c));
	
	draw_triangle_stroke(cr, a, b, c, str_a, str_b, str_c);
    }
    
    cairo_set_source_rgb(cr, 255, 0, 0);
    
    for(const Triangle& fh : not_faces)
    {		
	const Point &a(tr_and_scale(fh.vertex(0))),
	    &b(tr_and_scale(fh.vertex(1))),
	    &c(tr_and_scale(fh.vertex(2)));

	const unsigned lvl_a(kh.lvls[fh.vertex(0)].lvl),
	    lvl_b(kh.lvls[fh.vertex(1)].lvl),
	    lvl_c(kh.lvls[fh.vertex(2)].lvl);

	const std::string str_a(std::to_string(lvl_a)),
	    str_b(std::to_string(lvl_b)),
	    str_c(std::to_string(lvl_c));
	
	draw_triangle_stroke(cr, a, b, c, str_a, str_b, str_c);
    }

    
    cairo_surface_destroy(surface);
    cairo_destroy(cr);    
}


#endif

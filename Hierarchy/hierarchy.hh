#ifndef HIERARCHY_HH
#define HIERARCHY_HH

#include <CGAL/Real_embeddable_traits.h>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/LU>
#include <eigen3/Eigen/Geometry>

#include <set>
#include <unordered_map>
#include <functional>
#include <iostream>
#include <stack>
#include <deque>

#include "../Triangulation/triangulation.hh"


template<class T> class Kirkpatrick_hierarchy
{
public:
    typedef T Triangulation;
    typedef typename T::Face_handle Face_handle;
    typedef typename T::Vertex_handle Vertex_handle;
    typedef typename T::Edge Trg_edge;
    typedef typename T::All_vertices_iterator All_vertices_iterator;
    typedef typename T::Finite_vertices_iterator Finite_vertices_iterator;
    typedef typename T::Vertex_circulator Vertex_circulator;
    typedef typename T::Face_circulator Face_circulator;
    typedef typename T::Locate_type Locate_type;
    
    typedef typename T::Geom_traits K;
    typedef typename K::Point_2 Point;
    typedef typename K::Triangle_2 Triangle;
    typedef typename K::Line_2 Line;
    typedef typename K::Circle_2 Circle;
    
    typedef typename K::RT Expr;
    typedef typename CGAL::Real_embeddable_traits<Expr>::To_double
    To_double;
    typedef typename CGAL::Real_embeddable_traits<Expr>::Abs Abs;

    typedef Eigen::Matrix<Expr, 2, 2> Matrix_22;
    typedef Eigen::Matrix<Expr, 2, 1> Vector_2;
    typedef Eigen::FullPivLU<Matrix_22> Full_LU_3;
    
    static To_double to_double;
    
    struct Hash_point
    {
	std::size_t operator()(const Point& p) const noexcept
	{
	    //std::cout << p.x() << "," << p.y() << std::endl;
	    std::size_t h1(std::hash<double>{}(static_cast<double>(p.x()))),
		h2(std::hash<double>{}(static_cast<double>(p.y())));

	    return h1 ^ (h2 << 1);
	}
    };

    struct Node
    {
	unsigned p;
	std::vector<unsigned> neighb;
    };

    typedef std::vector<Node> Graph;
    
    struct Lvl_info
    {
	unsigned lvl;
	std::vector<unsigned> node;

	Lvl_info() : lvl(0)
	{
	}
    };

    struct Edge
    {
	unsigned a, b;

	Edge() : a(0), b(0)
	{
	}
	
	Edge(unsigned a, unsigned b) : a(a), b(b)
	{
	}
	
	bool operator==(const Edge& other) const
	{
	    return (a == other.a && b == other.b)
		|| (a == other.b && b == other.a);
	}

	Edge& operator=(const Edge& other)
	{
	    a = other.a;
	    b = other.b;

	    return (*this);
	}
    };

    struct In_conflict
    {
	bool operator()(const Delaunay_triangulation& t, const Triangle& trg,
			const Point& p) const
	{
	    CGAL::Orientation o(trg.orientation());

	    Point a, b, c;

	    if(o == CGAL::Orientation::POSITIVE)
	    {
		a = trg[0];
		b = trg[1];
		c = trg[2];
	    }
	    else
	    {
		a = trg[0];
		b = trg[2];
		c = trg[1];
	    }

	    typename K::Side_of_oriented_circle_2 in_circle
		(K().side_of_oriented_circle_2_object());

	    CGAL::Oriented_side os(in_circle(a, b, c, p));

	    return os == CGAL::Oriented_side::ON_POSITIVE_SIDE;
	}

	bool operator()(const TD_Delaunay_triangulation& t, const Triangle& trg,
			const Point& p) const
	{
	    return t.in_circum_triangle(trg, p);
	}
	    
    };
    
    typedef std::unordered_map<Point, Lvl_info, Hash_point> Levels;
    typedef std::set<Vertex_handle> Indep_set;
    typedef std::vector<Graph> Hierarchy;
    typedef std::unordered_map<Point, unsigned, Hash_point> Point_indexes;
    typedef std::unordered_map<unsigned, unsigned> Appearances;
    typedef std::set<unsigned> Neighbors;
    typedef std::unordered_map<Point, Neighbors, Hash_point>
    Neighborhood;

    Hierarchy h;
    Levels lvls;
    std::vector<Point> all_pts;
    Point_indexes pts_ind;
    Neighborhood neighborhood;
    In_conflict in_conflict;
    Abs abs;
    
    Point a = Point(1, 2), b = Point(2, -0.5),
	c = Point(-1, -0.25);
    unsigned ind_a, ind_b, ind_c;
        
    Kirkpatrick_hierarchy(const Triangulation& t, unsigned max_d,
			  unsigned n);

    void path(const Point& s, const Point& t,
	      std::vector<Triangle>& faces,
	      std::vector<Triangle>& not_faces,
	      std::vector<std::vector<Triangle>>& extra_faces);
    void path_small_steps(const Point& s, const Point& t,
			  std::vector<Triangle>& faces,
			  std::vector<Triangle>& not_faces,
			  std::vector<std::vector<Triangle>>& extra_faces);
    void routing_path(const Point& s, const Point& t,
		      std::vector<Triangle>& faces,
		      std::vector<Triangle>& not_faces);
    void ad_hoc_routing_progress(const Point& s, const Point& t,
			std::vector<Triangle>& faces,
			std::vector<Triangle>& not_faces);
    void ad_hoc_routing_recursive(const Point& s, const Point& t,
				  std::vector<Triangle>& faces,
				  std::vector<Triangle>& not_faces);
    void routing_path_backtrack(const Point& s, const Point& t,
				std::vector<Triangle>& faces,
				std::vector<Triangle>& not_faces);
    void ad_hoc_routing_pov(const Point& s, const Point& t,
			    std::vector<Triangle>& faces,
			    std::vector<Triangle>& not_faces);
    
private:
    unsigned max_d;

    const Expr half = 0.5, expr_one = 1, third = 1 / 3;
    
    void select_independent_set(Triangulation& trg, Indep_set& s, unsigned i);
    unsigned degree(Triangulation& trg, Vertex_handle v);
    unsigned degree_unmarked(Triangulation& trg, Vertex_handle v);

    bool intersects_st(const Vector_2& s, const Vector_2& ts,
		       const Point& A, const Point& B) const;
    bool intersects_st(const Vector_2& s, const Vector_2& ts,
		       const Point& A, const Point& B,
		       Expr& lambda) const;
    bool is_trivial(const Point& s, const Point& t,
		    std::vector<Triangle>& faces);
    bool is_adjacent(const Node& n_s, const Point& s, const Point& t,
		     std::vector<Triangle>& faces);
    void init_path(const Point& s, const Point& t,
		   Edge& e_s, Edge& e_t, const Vector_2& vect_s,
		   const Vector_2& vect_t, const Vector_2& st,
		   std::vector<Triangle>& faces);
    void init_edge_and_face(const Node& n_s, const Point& s,
			    const Vector_2& vect_s, const Vector_2& ts, 
			    Edge& e_s, std::vector<Triangle>& faces);
    unsigned lvl(const Edge& e);
    void get_new_face(unsigned curr_lvl, Edge& e,
		      const Vector_2& vect_s, const Vector_2& ts,
		      std::vector<Triangle>& faces);
    void find_new_face(unsigned d, const Node& n,
		       const Point& other, const Point& a,
		       const Point& b, Edge& e,
		       const Vector_2& vect_s, const Vector_2& ts,
		       std::vector<Triangle>& faces);

    
    bool routing_is_adjacent(unsigned curr_lvl,
			     const Edge& e, const Point& t,
			     std::deque<Edge>& steps,
			     std::vector<Triangle>& faces);
    bool routing_is_trivial(const Point& s, const Point& t,
			    std::vector<Triangle>& faces);
    void routing_init_path(const Point& s, const Point& t,
			   std::deque<Edge>& steps, const Vector_2& vect_s,
			   const Vector_2& ts, std::vector<Triangle>& faces);
    void routing_get_new_face_backtrack(
	unsigned& curr_lvl, Edge& e, const Point& t,
	const Vector_2& vect_s, const Vector_2& ts,
	bool& go_up, std::stack<Edge>& edges,
	std::vector<Triangle>& faces,
	std::vector<Triangle>& not_faces);
    void routing_find_new_face_up_backtrack(
	unsigned d, const Node& n,
	const Point& other, const Point& a,
	const Point& b, Edge& e, const Point& t,
	const Vector_2& vect_s, const Vector_2& ts,
	bool& go_up,
	std::vector<Triangle>& faces,
	std::vector<Triangle>& not_faces);
    void routing_find_new_face_down_backtrack(
	unsigned d, const Node& n,
	const Point& other, const Point& a,
	const Point& b, Edge& e, const Point& t,
	const Vector_2& vect_s, const Vector_2& ts,
	std::stack<Edge>& edges, unsigned& curr_lvl,
	std::vector<Triangle>& faces,
	std::vector<Triangle>& not_faces);

    void routing_get_new_face(unsigned& curr_lvl, std::deque<Edge>& steps,
			      const Point& t,
			      const Vector_2& vect_s, const Vector_2& ts,
			      bool& go_up, 
			      std::vector<Triangle>& faces,
			      std::vector<Triangle>& not_faces);
    void routing_find_new_face_up(
	unsigned d, const Node& n,
	const Point& other, const Point& a,
	const Point& b, Point& c, Edge& e,
	std::deque<Edge>& steps, const Point& t,
	const Vector_2& vect_s, const Vector_2& ts,
	bool& go_up,
	std::vector<Triangle>& faces,
	std::vector<Triangle>& not_faces);
    bool routing_find_new_face_down(
	unsigned d, Node n,
	const Point& other, Point a,
	Point b, Point& c, Edge& e,
	std::deque<Edge>& steps, const Point& t,
	const Vector_2& vect_s, const Vector_2& ts,
	unsigned& curr_lvl,
	std::vector<Triangle>& faces,
	std::vector<Triangle>& not_faces);

    unsigned find_common_neighb(const Node& n_a, const Node& n_b,
				const Node& n_c, const unsigned& curr_lvl,
				unsigned& i_a, unsigned& i_b);

    void path_step(std::deque<Edge>& steps, const Edge& new_e,
		   std::vector<Triangle>& faces);
    Triangle induced_triangle(const Edge& e1, const Edge& e2);
    void change_edge(Point& a, Point& b, Point& c,
		     unsigned& i, unsigned& d, Edge& e, Node& n,
		     std::deque<Edge>& steps,
		     const Vector_2& vect_s, const Vector_2& ts,
		     unsigned& curr_lvl, std::vector<Triangle>& not_faces);

    void get_candidate(Expr& lambda, Edge& cand,
		       const Point& a, const Point& b,
		       const Point& p, const Edge& e,
		       const Vector_2& vect_s, const Vector_2& ts);
    bool is_neighbor(const Node& n, unsigned pt, unsigned& i);
    Node& get_node(unsigned pt, unsigned lvl);
    Node& get_node(const Point& p, unsigned lvl);

    void ad_hoc_init(const Point& s, const Point& t,
		     const Vector_2& vect_s, const Vector_2& vect_t,
		     const Vector_2& st, Edge& e,
		     std::vector<Triangle>& faces);
    bool ad_hoc_is_trivial(const Point& s, const Point& t,
			   std::vector<Triangle>& faces);
    bool ad_hoc_is_adjacent(const Point& t, const Edge& e,
			    std::vector<Triangle>& faces);

    unsigned lvl(unsigned p);
    unsigned lvl(const Point& p);
    unsigned min(unsigned a, unsigned b);

    void ad_hoc_find_new_face_up(
    const Point& a, const Point& b,
    Edge& e, Expr& lambda, unsigned lvl_e, const Point& t,
    const Vector_2& vect_t, const Vector_2& st, bool& go_up,
    std::vector<Triangle>& faces, std::vector<Triangle>& not_faces);

    void ad_hoc_get_candidate(
	const Point& a, const Point& b, const Point& t, const Edge& e,
	unsigned neighb, Edge& curr_e, Expr& lambda, 
	Expr& mu, Expr& new_lambda, unsigned lvl_e, 
	const Vector_2& vect_t, const Vector_2& vect_s, const Vector_2& st);
    void ad_hoc_get_candidate(
	const Point &a, const Point& b, const Point& t, unsigned neighb,
	Edge &curr_e, Expr &lambda, Expr& mu, Expr& new_lambda,
	const Vector_2 &vect_t, const Vector_2& vect_s, const Vector_2 &st);
    void ad_hoc_up_get_candidate(
	Expr &lambda, Edge &cand, const Point &a, const Point &b,
	const Point &p, const Edge &e, const Vector_2 &vect_t,
	const Vector_2 &st);
    
    bool ad_hoc_intersects_st(
	const Vector_2& t, const Vector_2& s, const Vector_2& st,
	const Point& A, const Point& B,
	Expr& lambda, Expr& mu);

    void ad_hoc_find_new_face_down(
	const Point& a, const Point& b, Edge& e, Expr& lambda,
	const Point& t, const Vector_2& vect_t, const Vector_2& st,
	std::vector<Triangle>& faces, std::vector<Triangle>& not_faces);
    void ad_hoc_rec_find_new_face_up(
	const Point& a, const Point& b,
	Edge& e, Expr& lambda, unsigned lvl_e, const Point& t,
	const Vector_2& vect_s, const Vector_2& vect_t,
	const Vector_2& st, const Vector_2& perp_st, const Line& l_st,
	bool& go_up,
	std::vector<Triangle>& faces, std::vector<Triangle>& not_faces);

    Point furthest_along_st(const Point& A, const Point& B, const Vector_2& s,
			    const Vector_2& st);
    Expr dist_along_st(const Point& p, const Vector_2& s,
		       const Vector_2& st);
    Triangle induced_triangle(const Edge& e, const Point& p);
    Expr max(Expr e1, Expr e2);
    Point furthest_along_st(const Point& A, const Point& B, const Point& C,
			    const Vector_2& s, const Vector_2& st);
    
    bool is_risky(const Point& a, const Point& b, const Point& p,
		  const Point& t, const Line& st, const Vector_2& perp_st,
		  const Vector_2& vect_s, const Vector_2& vect_st);
    bool is_risky(const Circle& old_circle, const Point& new_a,
		  const Point& new_b);

    bool intersect(const Edge& e1, const Edge& e2);

    void ad_hoc_pov_find_new_face_up(
	const Point& a, const Point& b,
	Edge& e, Expr& lambda, unsigned lvl_e, const Point& t,
	const Vector_2& vect_s, const Vector_2& vect_t, const Vector_2& st,
	const Vector_2& perp_st, const Line& l_st, bool& go_up,
	std::vector<Triangle>& faces, std::vector<Triangle>& not_faces);

    unsigned max_lvl(const Edge& e);
};

// Implementation below this point

template<class T>
Kirkpatrick_hierarchy<T>::Kirkpatrick_hierarchy(
    const T& t, unsigned max_d, unsigned n) : max_d(max_d)
{
    unsigned i(0);

    Triangulation curr_t(t);
    
    curr_t.insert(a);
    curr_t.insert(b);
    curr_t.insert(c);

    for(Finite_vertices_iterator it(curr_t.finite_vertices_begin()),
	    itend(curr_t.finite_vertices_end()); it != itend; ++it)
    {
	all_pts.push_back(it->point());
	pts_ind.insert(std::make_pair(it->point(), i));
	++i;
    }

    ind_a = pts_ind[a];
    ind_b = pts_ind[b];
    ind_c = pts_ind[c];
    
    i = 0;

    for(const Point& p : all_pts)
    {
	lvls.insert(std::make_pair(p, Lvl_info()));
    }

    while(n > 0)
    {
	std::cout << n << std::endl;
	
	Indep_set s;

	std::vector<Point> new_pts;

	h.push_back(Graph());
	
	select_independent_set(curr_t, s, i);

	n -= s.size();
	
	for(Finite_vertices_iterator it(curr_t.finite_vertices_begin()),
		itend(curr_t.finite_vertices_end()); it != itend; ++it)
	{
	    Vertex_circulator vc(curr_t.incident_vertices(it)), done(vc);

	    Node n;

	    const Point& p(it->point());

	    n.p = pts_ind[p];

	    do
	    {
		if(curr_t.is_infinite(vc))
		    continue;
		
		const Point& q(vc->point());
		
		unsigned i_q(pts_ind[q]);
		
		n.neighb.push_back(i_q);
	    } while(++vc != done);
	    
	    h[i].push_back(n);
	    lvls[p].node.push_back(h[i].size() - 1);
	    
	    if(s.find(it) != s.end())
	    {
		lvls[p].lvl = i;
		continue;
	    }

	    new_pts.push_back(p);
	}

	++i;
	curr_t = Triangulation();
	curr_t.insert(new_pts.begin(), new_pts.end());
    }

    h.push_back(Graph());
	
    for(Finite_vertices_iterator it(curr_t.finite_vertices_begin()),
	    itend(curr_t.finite_vertices_end()); it != itend; ++it)
    {
	Vertex_circulator vc(curr_t.incident_vertices(it)), done(vc);

	Node n;

	const Point& p(it->point());

	n.p = pts_ind[p];
	    
	do
	{
	    const Point& q(vc->point());

	    unsigned i_q(pts_ind[q]);

	    n.neighb.push_back(i_q);
	} while(++vc != done);

	h[i].push_back(n);
	lvls[p].node.push_back(h[i].size() - 1);
	lvls[p].lvl = i;
    }

    for(const Point& p : all_pts)
    {
	const Lvl_info& info_p(lvls[p]);

	neighborhood.insert(std::make_pair(p, Neighbors()));

	for(unsigned i(0); i <= info_p.lvl; ++i)
	{
	    const Node& n_p(get_node(p, i));

	    for(unsigned neighb : n_p.neighb)
	    {
		if(neighborhood[p].find(neighb) == neighborhood[p].end())
		    neighborhood[p].insert(neighb);
	    }
	}
    }
}


template<class T>
void Kirkpatrick_hierarchy<T>::select_independent_set(
    Triangulation& trg, Indep_set& s, unsigned i)
{
    bool all_marked(false);

    for(All_vertices_iterator it(trg.all_vertices_begin()),
	    itend(trg.all_vertices_end()); it != itend; ++it)
    {
	if(trg.is_infinite(it) || degree(trg, it) > max_d)
	    it->info().marked = true;
    }
    
    while(!all_marked)
    {
	Finite_vertices_iterator it(trg.finite_vertices_begin()),
	    itend(trg.finite_vertices_end());

	for(; (it->info().marked || it->point() == a || it->point() == b
		|| it->point() == c) && it != itend; ++it);

	if(it == itend)
	    break;
	
	unsigned min_deg(degree_unmarked(trg, it)), deg;

	Vertex_handle min_v(it);

	all_marked = true;

	for(; it != itend; ++it)
	{
	    if(it->point() == a || it->point() == b || it->point() == c)
		continue;
	    
	    V_info& info(it->info());

	    if(info.marked)
		continue;

	    all_marked = false;
	    deg = degree_unmarked(trg, it);
	    
	    if(deg < min_deg)
	    {
		min_deg = deg;
		min_v = it;
	    }
	}

	Vertex_circulator vc(trg.incident_vertices(min_v)), done(vc);

	min_v->info().marked = true;
	s.insert(min_v);

	do
	{
	    vc->info().marked = true;
	} while(++vc != done);
    }
}

template<class T>
unsigned Kirkpatrick_hierarchy<T>::degree(Triangulation& trg, Vertex_handle v)
{
    Vertex_circulator vc(trg.incident_vertices(v)), done(vc);

    unsigned d(0);

    do
    {
	++d;
    } while(++vc != done);
    
    return d;
}

template<class T>
unsigned Kirkpatrick_hierarchy<T>::degree_unmarked(
    Triangulation& trg, Vertex_handle v)
{
    Vertex_circulator vc(trg.incident_vertices(v)), done(vc);

    unsigned d(0);

    do
    {
	if(!vc->info().marked)
	    ++d;
    } while(++vc != done);
    
    return d;
}

template<class T>
bool Kirkpatrick_hierarchy<T>::intersects_st(
    const Vector_2 &s, const Vector_2 &ts, const Point &A, const Point &B) const
{
    const Vector_2 new_a(A.x(), A.y()),
	new_b(B.x(), B.y()),
	new_s(s - new_b),
	c(new_a - new_b);

    Matrix_22 m;

    m << c, ts;

    Vector_2 solution(m.fullPivLu().solve(new_s));

    return ((m * solution).isApprox(new_s) && solution(0) >= 0
	    && solution(0) <= 1 && solution(1) >= 0);
}

template<class T>
bool Kirkpatrick_hierarchy<T>::intersects_st(
    const Vector_2 &s, const Vector_2 &ts, const Point &A, const Point &B,
    Expr &lambda) const
{
    const Vector_2 new_a(A.x(), A.y()),
	new_b(B.x(), B.y()),
	new_s(s - new_b),
	c(new_a - new_b);

    Matrix_22 m;

    m << c, ts;

    Vector_2 solution(m.fullPivLu().solve(new_s));

    lambda = solution(1);
    
    return ((m * solution).isApprox(new_s) && solution(0) >= 0
	    && solution(0) <= 1 && solution(1) >= 0);
}


template<class T>
void Kirkpatrick_hierarchy<T>::path(
    const Point& s, const Point& t, std::vector<Triangle>& faces,
    std::vector<Triangle>& not_faces,
    std::vector<std::vector<Triangle>>& extra_faces)
{
    std::cout << "Path: " << std::endl;
    
    if(is_trivial(s, t, faces))
	return;

    const Vector_2 vect_s(s.x(), s.y()),
	vect_t(t.x(), t.y()),
	st(vect_t - vect_s);

    Edge e_s, e_t, dummy;

    init_path(s, t, e_s, e_t, vect_s, vect_t, st, faces);

    dummy = e_s;

    unsigned i(0);
    
    while(!(e_s == e_t))
    {
	const unsigned lvl_e_s(lvl(e_s)),
	    lvl_e_t(lvl(e_t));

	if(lvl_e_s <= lvl_e_t)
	{
	    get_new_face(lvl_e_s, e_s, vect_s, - st, faces);
	    
	    extra_faces.push_back(std::vector<Triangle>());
	
	    const Point &a(all_pts[e_s.a]),
		&b(all_pts[e_s.b]);

	    std::cout << lvl(a) << " ; " << lvl(b) << std::endl;
	    
 	    const Neighbors &neighbs_a(neighborhood[a]),
		&neighbs_b(neighborhood[b]);

	    unsigned lvl_e(lvl(e_s));
    
	    for(unsigned neighb : neighbs_a)
	    {
		if(neighbs_b.find(neighb) == neighbs_b.end())
		    continue;

		if((neighb == ind_a) || (neighb == ind_b)
		   || (neighb == ind_c))
		    continue;

		unsigned curr_lvl(min(lvl_e, lvl(neighb)));
	
		const Node &n_a(get_node(a, curr_lvl)),
		    &n_neighb(get_node(neighb, curr_lvl));

		unsigned i, j, k;

		if(!is_neighbor(n_a, e_s.b, i) || !is_neighbor(n_neighb, e_s.a, j)
		   || !is_neighbor(n_neighb, e_s.b, k))
		    continue;

		Triangle trg(a, b, all_pts[neighb]);

		extra_faces.back().push_back(trg);
	    }
	}
	else
	{
	    get_new_face(lvl_e_t, e_t, vect_t, st, faces);

	    extra_faces.push_back(std::vector<Triangle>());
	
	    const Point &a(all_pts[e_t.a]),
		&b(all_pts[e_t.b]);

	    std::cout << lvl(a) << " ; " << lvl(b) << std::endl;

 	    const Neighbors &neighbs_a(neighborhood[a]),
		&neighbs_b(neighborhood[b]);

	    unsigned lvl_e(lvl(e_t));
    
	    for(unsigned neighb : neighbs_a)
	    {
		if(neighbs_b.find(neighb) == neighbs_b.end())
		    continue;

		if((neighb == ind_a) || (neighb == ind_b)
		   || (neighb == ind_c))
		    continue;

		unsigned curr_lvl(min(lvl_e, lvl(neighb)));
	
		const Node &n_a(get_node(a, curr_lvl)),
		    &n_neighb(get_node(neighb, curr_lvl));

		unsigned i, j, k;

		if(!is_neighbor(n_a, e_t.b, i) || !is_neighbor(n_neighb, e_t.a, j)
		   || !is_neighbor(n_neighb, e_t.b, k))
		    continue;

		Triangle trg(a, b, all_pts[neighb]);

		extra_faces.back().push_back(trg);
	    }
	}
    }
}

template<class T>
void Kirkpatrick_hierarchy<T>::path_small_steps(
    const Point& s, const Point& t, std::vector<Triangle>& faces,
    std::vector<Triangle>& not_faces,
    std::vector<std::vector<Triangle>>& extra_faces)
{
    std::cout << "Path: " << std::endl;
    
    if(is_trivial(s, t, faces))
	return;

    const Vector_2 vect_s(s.x(), s.y()),
	vect_t(t.x(), t.y()),
	st(vect_t - vect_s);

    Edge e_s, e_t, dummy;

    init_path(s, t, e_s, e_t, vect_s, vect_t, st, faces);

    dummy = e_s;

    unsigned i(0), lvl_e_s(0), lvl_e_t(0);
    
    while(!(e_s == e_t))
    {
	if(lvl_e_s <= lvl_e_t)
	{
	    const Edge old_e_s(e_s);
	    
	    get_new_face(lvl_e_s, e_s, vect_s, - st, faces);

	    if(lvl(e_s) > lvl(old_e_s) || lvl(old_e_s) != lvl_e_s)
		++lvl_e_s;
	    
	    extra_faces.push_back(std::vector<Triangle>());
	
	    const Point &a(all_pts[e_s.a]),
		&b(all_pts[e_s.b]);

	    std::cout << lvl(a) << " ; " << lvl(b) << std::endl;
	    
 	    const Neighbors &neighbs_a(neighborhood[a]),
		&neighbs_b(neighborhood[b]);

	    unsigned lvl_e(lvl(e_s));
    
	    for(unsigned neighb : neighbs_a)
	    {
		if(neighbs_b.find(neighb) == neighbs_b.end())
		    continue;

		if((neighb == ind_a) || (neighb == ind_b)
		   || (neighb == ind_c))
		    continue;

		unsigned curr_lvl(min(lvl_e, lvl(neighb)));
	
		const Node &n_a(get_node(a, curr_lvl)),
		    &n_neighb(get_node(neighb, curr_lvl));

		unsigned i, j, k;

		if(!is_neighbor(n_a, e_s.b, i) || !is_neighbor(n_neighb, e_s.a, j)
		   || !is_neighbor(n_neighb, e_s.b, k))
		    continue;

		Triangle trg(a, b, all_pts[neighb]);

		extra_faces.back().push_back(trg);
	    }
	}
	else
	{
	    const Edge old_e_t(e_t);
	    
	    get_new_face(lvl_e_t, e_t, vect_t, st, faces);

	    if(lvl(e_t) > lvl(old_e_t) || lvl(old_e_t) != lvl_e_t)
		++lvl_e_t;
	    
	    extra_faces.push_back(std::vector<Triangle>());
	
	    const Point &a(all_pts[e_t.a]),
		&b(all_pts[e_t.b]);

	    std::cout << lvl(a) << " ; " << lvl(b) << std::endl;

 	    const Neighbors &neighbs_a(neighborhood[a]),
		&neighbs_b(neighborhood[b]);

	    unsigned lvl_e(lvl(e_t));
    
	    for(unsigned neighb : neighbs_a)
	    {
		if(neighbs_b.find(neighb) == neighbs_b.end())
		    continue;

		if((neighb == ind_a) || (neighb == ind_b)
		   || (neighb == ind_c))
		    continue;

		unsigned curr_lvl(min(lvl_e, lvl(neighb)));
	
		const Node &n_a(get_node(a, curr_lvl)),
		    &n_neighb(get_node(neighb, curr_lvl));

		unsigned i, j, k;

		if(!is_neighbor(n_a, e_t.b, i) || !is_neighbor(n_neighb, e_t.a, j)
		   || !is_neighbor(n_neighb, e_t.b, k))
		    continue;

		Triangle trg(a, b, all_pts[neighb]);

		extra_faces.back().push_back(trg);
	    }
	}
    }
}

template<class T>
void Kirkpatrick_hierarchy<T>::routing_path(const Point& s, const Point& t,
					    std::vector<Triangle>& faces,
					    std::vector<Triangle>& not_faces)
{
    if(routing_is_trivial(s, t, faces))
	return;

    const Vector_2 vect_s(s.x(), s.y()),
	vect_t(t.x(), t.y()),
	ts(vect_s - vect_t);

    //std::stack<Edge> edges;

    Edge e_s;
    
    std::deque<Edge> steps;
    
    bool go_up(true);

    routing_init_path(s, t, steps, vect_s, ts, faces);

    e_s = steps.back();
    
    unsigned i(0), curr_lvl(lvl(e_s)); 

    while(!routing_is_adjacent(curr_lvl, e_s, t, steps, faces) && i++ < 50)
    {
	std::cout << "In routing_path" << std::endl;
    
	routing_get_new_face(
	    curr_lvl, steps, t, vect_s, ts, go_up, faces,
	    not_faces);

	e_s = steps.back();
    }

    Triangle back(faces.back());

    faces.pop_back();

    for(i = 0; i < steps.size() - 1; ++i)
	faces.push_back(induced_triangle(steps[i], steps[i + 1]));

    faces.push_back(back);
}

template<class T>
bool Kirkpatrick_hierarchy<T>::is_trivial(
    const Point& s, const Point& t, std::vector<Triangle>& faces)
{
    const Lvl_info &info_s(lvls[s]),
	&info_t(lvls[t]);

    const Node &n_s(h[info_s.lvl][info_s.node[info_s.lvl]]),
	&n_t(h[info_t.lvl][info_t.node[info_t.lvl]]);

    if(is_adjacent(n_s, s, t, faces))
	return true;

    if(is_adjacent(n_t, t, s, faces))
	return true;

    return false;
}

template<class T>
bool Kirkpatrick_hierarchy<T>::is_adjacent(
    const Node &n_s, const Point& s,
    const Point &t, std::vector<Triangle> &faces)
{
    unsigned i(0);
    
    for(; i < n_s.neighb.size(); ++i)
    {
	const Point& p(all_pts[(n_s.neighb)[i]]);

	if(p == t)
	{
	    unsigned j;
	    
	    if(i + 1 < n_s.neighb.size())
		j = i + 1;
	    else
		j = 0;

	    const Point& p_j(all_pts[(n_s.neighb)[j]]);

	    const Triangle trg(s, t, p_j);
	    
	    faces.push_back(trg);

	    return true;
	}
    }

    return false;
}

template<class T>
void Kirkpatrick_hierarchy<T>::init_path(
    const Point& s, const Point& t, Edge& e_s, Edge& e_t,
    const Vector_2& vect_s, const Vector_2& vect_t, const Vector_2& st,
    std::vector<Triangle>& faces)
{
    const Node &n_s(get_node(s, 0)),
	&n_t(get_node(t, 0));

    init_edge_and_face(n_s, s, vect_s, - st, e_s, faces);
    init_edge_and_face(n_t, t, vect_t, st, e_t, faces);
}

template<class T>
void Kirkpatrick_hierarchy<T>::init_edge_and_face(
    const Node& n_s, const Point& s,
    const Vector_2 &vect_s, const Vector_2 &ts,
    Edge &e_s, std::vector<Triangle> &faces)
{
    unsigned i(0), size(n_s.neighb.size());

    for(; i < size; ++i)
    {
	unsigned j(i + 1 < size ? i + 1 : 0);

	const Point &curr(all_pts[(n_s.neighb)[i]]),
	    &next(all_pts[(n_s.neighb)[j]]);

	if(intersects_st(vect_s, ts, curr, next))
	{
	    e_s.a = pts_ind[curr];
	    e_s.b = pts_ind[next];

	    const Triangle trg(s, curr, next);

	    faces.push_back(trg);

	    return;
	}
    }
}

template<class T>
void Kirkpatrick_hierarchy<T>::get_new_face(
    unsigned curr_lvl, Edge &e, const Vector_2 &vect_s,
    const Vector_2 &ts, std::vector<Triangle> &faces)
{
    const Point &a(all_pts[e.a]),
	&b(all_pts[e.b]);
    
    const Node &n_a(get_node(a, curr_lvl)),
	&n_b(get_node(b, curr_lvl));

    unsigned d_a(n_a.neighb.size()),
	d_b(n_b.neighb.size());

    if(d_a < d_b)
	find_new_face(d_a, n_a, b, a, b, e, vect_s, ts, faces);
    else
	find_new_face(d_b, n_b, a, a, b, e, vect_s, ts, faces);
}

template<class T>
void Kirkpatrick_hierarchy<T>::find_new_face(
    unsigned d, const Node& n, const Point& other,
    const Point& a, const Point& b, Edge& e,
    const Vector_2& vect_s, const Vector_2& ts,
    std::vector<Triangle>& faces)
{
    unsigned i(0);

    is_neighbor(n, pts_ind[other], i);

    unsigned left(i > 0 ? i - 1 : d - 1),
	right(i + 1 < d ? i + 1 : 0);

    const Point &p_left(all_pts[n.neighb[left]]),
	&p_right(all_pts[n.neighb[right]]);

    Expr lambda_l, lambda_r;

    Edge cand_l, cand_r;

    get_candidate(lambda_l, cand_l, a, b, p_left, e, vect_s, ts);
    get_candidate(lambda_r, cand_r, a, b, p_right, e, vect_s, ts);
    
    Triangle trg;
    
    if(lambda_l > lambda_r)
    {
	e = cand_l;
	trg = Triangle(a, b, p_left);
    }
    else
    {
	e = cand_r;
	trg = Triangle(a, b, p_right);
    }

    faces.push_back(trg);
}

template<class T>
unsigned Kirkpatrick_hierarchy<T>::lvl(const Edge& e)
{
    const Point &a(all_pts[e.a]),
	&b(all_pts[e.b]);

    unsigned lvl_a(lvls[a].lvl),
	lvl_b(lvls[b].lvl);
    
    return lvl_a < lvl_b ? lvl_a : lvl_b;
}

template<class T>
bool Kirkpatrick_hierarchy<T>::routing_is_trivial(
    const Point& s, const Point& t, std::vector<Triangle>& faces)
{
    const Lvl_info &info_s(lvls[s]);

    const Node &n_s(h[info_s.lvl][info_s.node[info_s.lvl]]);

    return is_adjacent(n_s, s, t, faces);
}

template<class T>
bool Kirkpatrick_hierarchy<T>::routing_is_adjacent(
    unsigned curr_lvl, const Edge &e, const Point &t,
    std::deque<Edge>& steps,
    std::vector<Triangle>& faces)
{
    const Point &a(all_pts[e.a]),
	&b(all_pts[e.b]);

    if(a == t || b == t)
	return true;

    const Node &n_a(get_node(a, curr_lvl)),
	&n_b(get_node(b, curr_lvl));

    const unsigned d_a(n_a.neighb.size()),
	d_b(n_b.neighb.size());

    bool adj(false);
    
    if(d_a < d_b)
	adj = is_adjacent(n_a, a, t, faces);
    else
	adj = is_adjacent(n_b, b, t, faces);

    if(adj)
	return true;

    const Node &n_a_0(get_node(a, 0)),
	&n_b_0(get_node(b, 0));

    adj = is_adjacent(n_a_0, a, t, faces);

    if(adj)
	return true;

    adj = is_adjacent(n_b_0, b, t, faces);

    return adj;
}

template<class T>
void Kirkpatrick_hierarchy<T>::routing_init_path(
    const Point& s, const Point& t,
    std::deque<Edge>& steps, const Vector_2& vect_s, const Vector_2& ts,
    std::vector<Triangle>& faces)
{
    const Node &n_s(get_node(s, 0));

    Edge e_s;
    
    init_edge_and_face(n_s, s, vect_s, ts, e_s, faces);

    steps.push_back(e_s);
}


template<class T>
void Kirkpatrick_hierarchy<T>::routing_get_new_face_backtrack(
    unsigned &curr_lvl, Edge &e, const Point& t, const Vector_2 &vect_s,
    const Vector_2 &ts, bool &go_up, std::stack<Edge>& edges,
    std::vector<Triangle> &faces, std::vector<Triangle>& not_faces)
{
    if(go_up)
    {
	const Point &a(all_pts[e.a]),
	    &b(all_pts[e.b]);
	
	const Node &n_a(get_node(a, curr_lvl)),
	    &n_b(get_node(b, curr_lvl));

	unsigned d_a(n_a.neighb.size()),
	    d_b(n_b.neighb.size());

	if(d_a < d_b)
	    routing_find_new_face_up_backtrack(
		d_a, n_a, b, a, b, e, t, vect_s, ts, go_up, faces, not_faces);
	else
	    routing_find_new_face_up_backtrack(
		d_b, n_b, a, a, b, e, t, vect_s, ts, go_up, faces, not_faces);

	curr_lvl = lvl(e);
    }

    if(!go_up)
    {
	while(e == edges.top())
	{
	    const Point &a(all_pts[e.a]),
		&b(all_pts[e.b]);

	    const Node &n_a(get_node(a, curr_lvl)),
		&n_b(get_node(b, curr_lvl));

	    unsigned d_a(n_a.neighb.size()),
		d_b(n_b.neighb.size());

	    if(d_a < d_b)
		routing_find_new_face_down_backtrack(
		    d_a, n_a, b, a, b, e, t, vect_s, ts,
		    edges, curr_lvl, faces, not_faces);
	    else
		routing_find_new_face_down_backtrack(
		    d_b, n_b, a, a, b, e, t, vect_s, ts,
		    edges, curr_lvl, faces, not_faces);
	}
    }
}

template<class T>
void Kirkpatrick_hierarchy<T>::routing_find_new_face_up_backtrack(
    unsigned d, const Node& n, const Point& other,
    const Point& a, const Point& b, Edge& e, const Point& t,
    const Vector_2& vect_s, const Vector_2& ts, bool& go_up,
    std::vector<Triangle>& faces, std::vector<Triangle>& not_faces)
{
    unsigned i(0);

    is_neighbor(n, pts_ind[other], i);

    unsigned left(i > 0 ? i - 1 : d - 1),
	right(i + 1 < d ? i + 1 : 0);

    const Point &p_left(all_pts[n.neighb[left]]),
	&p_right(all_pts[n.neighb[right]]);

    Expr lambda_l, lambda_r;

    Edge cand_l, cand_r;
    
    get_candidate(lambda_l, cand_l, a, b, p_left, e, vect_s, ts);
    get_candidate(lambda_r, cand_r, a, b, p_right, e, vect_s, ts);

    Triangle trg;
    
    if(lambda_l > lambda_r)
    {
	trg = Triangle(a, b, p_left);
	
	if(in_conflict(Triangulation(), trg, t))
	{
	    not_faces.push_back(trg);
	    go_up = false;
	    return;
	}	

	faces.push_back(trg);
	e = cand_l;
    }
    else
    {
	trg = Triangle(a, b, p_right);
	
	if(in_conflict(Triangulation(), trg, t))
	{
	    not_faces.push_back(trg) ;
	    go_up = false;
	    return;
	}

	faces.push_back(trg);
	e = cand_r;
    }
}

template<class T>
void Kirkpatrick_hierarchy<T>::routing_find_new_face_down_backtrack(
    unsigned d, const Node& n, const Point& other,
    const Point& a, const Point& b, Edge& e, const Point& t,
    const Vector_2& vect_s, const Vector_2& ts,
    std::stack<Edge>& edges, unsigned& curr_lvl,
    std::vector<Triangle>& faces, std::vector<Triangle>& not_faces)
{
    unsigned i(0);

    if(!is_neighbor(n, pts_ind[other], i))
    {
	edges.pop();

	if(!edges.empty())
	{
	    std::cout << "Backtracking..." << std::endl;

	    e = edges.top();
	    const Triangle& back(faces.back());

	    not_faces.push_back(back);
	    
	    faces.pop_back();
	}
	else
	    edges.push(e);
	
	return;
    }

    unsigned left(i > 0 ? i - 1 : d - 1),
	right(i + 1 < d ? i + 1 : 0);

    const Point &p_left(all_pts[n.neighb[left]]),
	&p_right(all_pts[n.neighb[right]]);

    Expr lambda_l, lambda_r;

    Edge cand_l, cand_r;

    get_candidate(lambda_l, cand_l, a, b, p_left, e, vect_s, ts);
    get_candidate(lambda_r, cand_r, a, b, p_right, e, vect_s, ts);
    
    Triangle trg;
    
    if(lambda_l > lambda_r)
    {
	trg = Triangle(a, b, p_left);
	
	if(in_conflict(Triangulation(), trg, t))
	{
	    not_faces.push_back(trg);
	    
	    curr_lvl = curr_lvl > 1 ? curr_lvl - 1 : 0;
	    return;
	}

	e = cand_l;
    }
    else
    {
	trg = Triangle(a, b, p_right);

	if(in_conflict(Triangulation(), trg, t))
	{
	    not_faces.push_back(trg);
	    
	    curr_lvl = curr_lvl > 1 ? curr_lvl - 1 : 0;
	    return;
	}	
	
	e = cand_r;
    }

    faces.push_back(trg);
}

template<class T>
void Kirkpatrick_hierarchy<T>::routing_get_new_face(
    unsigned int &curr_lvl, std::deque<Edge>& steps, const Point &t,
    const Vector_2 &vect_s, const Vector_2 &ts, bool &go_up,
    std::vector<Triangle> &faces, std::vector<Triangle> &not_faces)
{
    Point c;

    Edge e(steps.back());
    
    if(go_up)
    {
	curr_lvl = lvl(e);
	
	const Point &a(all_pts[e.a]),
	    &b(all_pts[e.b]);

	std::cout << lvls[a].lvl << " ; " << lvls[b].lvl << std::endl;
	
	const Node &n_a(get_node(a, curr_lvl)),
	    &n_b(get_node(b, curr_lvl));

	unsigned d_a(n_a.neighb.size()),
	    d_b(n_b.neighb.size());

	if(d_a < d_b)
	    routing_find_new_face_up(
		d_a, n_a, b, a, b, c, e, steps, t, vect_s,
		ts, go_up, faces, not_faces);
	else
	    routing_find_new_face_up(
		d_b, n_b, a, a, b, c, e, steps, t, vect_s,
		ts, go_up, faces, not_faces);
    }

    if(!go_up)
    {
	bool not_done(true);
	
	while(not_done)
	{
	    const Point &a(all_pts[e.a]),
		&b(all_pts[e.b]);
	    
	    std::cout << lvls[a].lvl << " ; " << lvls[b].lvl << std::endl;
	
	    const Node &n_a(get_node(a, curr_lvl)),
		&n_b(get_node(b, curr_lvl));

	    unsigned d_a(n_a.neighb.size()),
		d_b(n_b.neighb.size());

	    if(d_a < d_b)
		not_done = routing_find_new_face_down(
		    d_a, n_a, b, a, b, c, e, steps, t, vect_s,
		    ts, curr_lvl, faces, not_faces);
	    else
		not_done = routing_find_new_face_down(
		    d_b, n_b, a, a, b, c, e, steps, t, vect_s,
		    ts, curr_lvl, faces, not_faces);
	}
    }
}

template<class T>
void Kirkpatrick_hierarchy<T>::routing_find_new_face_up(
    unsigned int d, const Node &n, const Point &other, const Point &a,
    const Point &b, Point &c, Edge &e, std::deque<Edge>& steps,
    const Point &t, const Vector_2 &vect_s,
    const Vector_2 &ts, bool &go_up,
    std::vector<Triangle> &faces, std::vector<Triangle> &not_faces)
{
    unsigned i(0);

    is_neighbor(n, pts_ind[other], i);

    unsigned left(i > 0 ? i - 1 : d - 1),
	right(i + 1 < d ? i + 1 : 0);

    const Point &p_left(all_pts[n.neighb[left]]),
	&p_right(all_pts[n.neighb[right]]);

    Expr lambda_l, lambda_r;

    Edge cand_l, cand_r;
    
    get_candidate(lambda_l, cand_l, a, b, p_left, e, vect_s, ts);
    get_candidate(lambda_r, cand_r, a, b, p_right, e, vect_s, ts);
    
    Triangle trg;
    
    if(lambda_l > lambda_r)
    {
	trg = Triangle(a, b, p_left);
	
	if(in_conflict(Triangulation(), trg, t))
	{
	    not_faces.push_back(trg);
	    go_up = false;
	    c = p_left;
	    return;
	}	

	path_step(steps, cand_l, faces);
    }
    else
    {
	trg = Triangle(a, b, p_right);
	
	if(in_conflict(Triangulation(), trg, t))
	{
	    not_faces.push_back(trg) ;
	    go_up = false;
	    c = p_right;
	    return;
	}

	path_step(steps, cand_r, faces);
    }
}

template<class T>
bool Kirkpatrick_hierarchy<T>::routing_find_new_face_down(
    unsigned d, Node n, const Point& other, Point a, Point b,
    Point& c, Edge& e, std::deque<Edge>& steps, const Point& t,
    const Vector_2& vect_s, const Vector_2& ts, unsigned& curr_lvl,
    std::vector<Triangle>& faces, std::vector<Triangle>& not_faces)
{
    unsigned i(0);

    if(!is_neighbor(n, pts_ind[other], i))
    {
	std::cout << "Change " << std::endl;
	change_edge(a, b, c, i, d, e, n, steps, vect_s, ts,
		    curr_lvl, not_faces);
    }

    unsigned left(i > 0 ? i - 1 : d - 1),
	right(i + 1 < d ? i + 1 : 0);

    const Point &p_left(all_pts[n.neighb[left]]),
	&p_right(all_pts[n.neighb[right]]);

    Expr lambda_l, lambda_r;

    Edge cand_l, cand_r;
    
    get_candidate(lambda_l, cand_l, a, b, p_left, e, vect_s, ts);
    get_candidate(lambda_r, cand_r, a, b, p_right, e, vect_s, ts);

    Triangle trg;
    
    if(lambda_l > lambda_r)
    {
	trg = Triangle(a, b, p_left);
	c = p_left;
	
	if(in_conflict(Triangulation(), trg, t))
	{
	    not_faces.push_back(trg);
	    
	    curr_lvl = curr_lvl > 1 ? curr_lvl - 1 : 0;
	    return true;
	}

	path_step(steps, cand_l, faces);
    }
    else
    {
	trg = Triangle(a, b, p_right);
	c = p_right;
	
	if(in_conflict(Triangulation(), trg, t))
	{
	    not_faces.push_back(trg);
	    
	    curr_lvl = curr_lvl > 1 ? curr_lvl - 1 : 0;
	    return true;
	}	

	path_step(steps, cand_r, faces);
    }        

    return false;
}

template<class T>
unsigned Kirkpatrick_hierarchy<T>::find_common_neighb(
    const Node &n_a, const Node &n_b, const Node &n_c,
    const unsigned int &curr_lvl, unsigned int &i_a, unsigned int &i_b)
{
    Appearances app;

    unsigned common_neighb;

    for(const unsigned& neighb : n_a.neighb)
    {
	if(lvls[all_pts[neighb]].lvl == curr_lvl)
	    app[neighb] = 1;
    }

    for(const unsigned& neighb : n_b.neighb)
    {
	if(lvls[all_pts[neighb]].lvl == curr_lvl &&
	   app.find(neighb) != app.end())
	    app[neighb] = 2;
    }

    for(const unsigned& neighb : n_c.neighb)
    {
	if(lvls[all_pts[neighb]].lvl == curr_lvl &&
	   app.find(neighb) != app.end())
	    app[neighb] = app[neighb] + 1;
    }

    for(std::pair<unsigned, unsigned> vt : app)
    {
	if(vt.second == 3)
	{
	    std::cout << "all good" << std::endl;
	    common_neighb = vt.first;
	    break;
	}
    }

    for(i_a = 0; n_a.neighb[i_a] != common_neighb; ++i_a);

    for(i_b = 0; n_b.neighb[i_b] != common_neighb; ++i_b);

    return common_neighb;
}

template<class T>
void Kirkpatrick_hierarchy<T>::path_step(
    std::deque<Edge>& steps, const Edge& new_e, std::vector<Triangle>& faces)
{
    if(steps.size() < max_d - 2)
    {
	steps.push_back(new_e);
	return;
    }

    Triangle trg(induced_triangle(steps[0], steps[1]));

    faces.push_back(trg);

    steps.pop_front();
    steps.push_back(new_e);
}

template<class T>
typename Kirkpatrick_hierarchy<T>::Triangle
Kirkpatrick_hierarchy<T>::induced_triangle(const Edge &e1, const Edge &e2)
{
    unsigned a(e1.a), b(e1.b), c((e2.a == e1.a || e2.a == e1.b) ? e2.b : e2.a);

    const Point &pt_a(all_pts[a]),
	&pt_b(all_pts[b]),
	&pt_c(all_pts[c]);

    return Triangle(pt_a, pt_b, pt_c);
}

template<class T>
void Kirkpatrick_hierarchy<T>::change_edge(
    Point &a, Point &b, Point &c, unsigned int &i,
    unsigned int &d, Edge &e, Node &n, std::deque<Edge> &steps,
    const Vector_2 &vect_s, const Vector_2 &ts, unsigned int &curr_lvl,
    std::vector<Triangle>& not_faces)
{
    const Node &n_a(get_node(a, curr_lvl)),
	&n_b(get_node(b, curr_lvl)),
	&n_c(get_node(c, curr_lvl));

    unsigned i_a, i_b;
	
    const unsigned new_p(find_common_neighb(n_a, n_b, n_c, curr_lvl, i_a,
						i_b));

    const Point &p(all_pts[new_p]);
	
    if(intersects_st(vect_s, ts, a, p))
    {
	e.b = new_p;
	i = i_a;
	b = p;
	n = n_a;
	d = n.neighb.size();
    }
    else
    {
	e.a = new_p;
	i = i_b;
	a = p;
	n = n_b;
	d = n.neighb.size();
    }

    unsigned s(steps.size() - 1);

    std::deque<Edge> new_steps;

    new_steps.push_back(e);

    if(s > 0)
	--s;

    const Node &n_p(get_node(p, curr_lvl));
	
    while(s > 0)
    {
	std::cout << s << std::endl;

	const Edge& curr_e(steps[s]);

	if(lvl(curr_e) < curr_lvl)
	{
	    for(; s > 0; --s)
		new_steps.push_front(steps[s]);

	    new_steps.push_front(steps[0]);
	    break;
	}

	const Node &n_c_a(get_node(curr_e.a, curr_lvl)),
	    &n_c_b(get_node(curr_e.b, curr_lvl));

	bool exists(false);

	unsigned j;
	    
	if(n_c_a.neighb.size() < n_c_b.neighb.size())
	    exists = is_neighbor(n_c_a, curr_e.b, j);
	else
	    exists = is_neighbor(n_c_b, curr_e.a, j);

	if(exists)
	{
	    for(; s > 0; --s)
		new_steps.push_front(steps[s]);

	    new_steps.push_front(steps[0]);
	    break;
	}
	else
	{
	    const Edge& prev_e(steps[s - 1]);
	    
	    unsigned prev_a(prev_e.a), prev_b(prev_e.b), other(new_p);
	    
	    for(const unsigned v : n_p.neighb)
	    {
		if(v == prev_a || v == prev_b)
		{
		    other = v;
		    break;
		}
	    }
	    
	    not_faces.push_back(induced_triangle(prev_e, steps[s]));
		
	    if(intersects_st(vect_s, ts, all_pts[other], p))
		new_steps.push_front(Edge(other, new_p));

	    --s;
	}
    }

    steps = new_steps;
}

template<class T>
void Kirkpatrick_hierarchy<T>::get_candidate(
    Expr &lambda, Edge &cand, const Point &a, const Point &b,
    const Point &p, const Edge &e, const Vector_2 &vect_s, const Vector_2 &ts)
{
    if(intersects_st(vect_s, ts, a, p, lambda))
    {
	cand.a = e.a;
	cand.b = pts_ind[p];
    }
    else if(intersects_st(vect_s, ts, b, p, lambda))
    {
	cand.a = pts_ind[p];
	cand.b = e.b;
    }
    else
	lambda = -500;
}

template<class T>
bool Kirkpatrick_hierarchy<T>::is_neighbor(
    const Node& n, unsigned pt, unsigned& i)
{
    for(i = 0; i < n.neighb.size() && n.neighb[i] != pt; ++i);

    return i != n.neighb.size();
}

template<class T>
typename Kirkpatrick_hierarchy<T>::Node&
Kirkpatrick_hierarchy<T>::get_node(unsigned int pt, unsigned int lvl)
{
    const Point& p(all_pts[pt]);

    return get_node(p, lvl);
}

template<class T>
typename Kirkpatrick_hierarchy<T>::Node&
Kirkpatrick_hierarchy<T>::get_node(const Point& p, unsigned int lvl)
{
    const Lvl_info& info_p(lvls[p]);

    Node& n(h[lvl][info_p.node[lvl]]);

    return n;
}

template<class T>
void Kirkpatrick_hierarchy<T>::ad_hoc_routing_progress(
    const Point &s, const Point &t, std::vector<Triangle> &faces,
    std::vector<Triangle> &not_faces)
{
    std::cout << "Ad hoc:" << std::endl;
    
    const Vector_2 vect_t(t.x(), t.y()),
	vect_s(s.x(), s.y()),
	st(vect_t - vect_s);

    Edge e;

    Expr lambda, mu(-500), new_lambda;

    bool go_up(true);
    
    if(ad_hoc_is_trivial(s, t, faces))
	return;

    ad_hoc_init(s, t, vect_s, vect_t, st, e, faces);

    intersects_st(vect_t, st, all_pts[e.a], all_pts[e.b], lambda);

    unsigned i(0);

    std::cout << ind_a << " ; " << ind_b << " ; " << ind_c
	      << std::endl;

    go_up = lambda > half;
    
    while(!ad_hoc_is_adjacent(t, e, faces) && i++ < 200)
    {
	const Point &a(all_pts[e.a]),
	    &b(all_pts[e.b]);

	intersects_st(vect_t, st, a, b, lambda);
	
	std::cout << lvl(a) << " ; " << lvl(b) << std::endl;
	
	const Neighbors &neighbs_a(neighborhood[a]),
	    &neighbs_b(neighborhood[b]);

	const unsigned lvl_e(lvl(e));

	Edge curr_e(e);
	
	if(go_up)
	{
	    ad_hoc_find_new_face_up(a, b, curr_e, lambda, lvl_e, t, vect_t,
				    st, go_up, faces, not_faces);

	    if(!go_up)
		ad_hoc_intersects_st(vect_t, vect_s, st, all_pts[curr_e.a],
				     all_pts[curr_e.b], lambda, mu);
	}
	else
	{
	    bool first(true);

	    mu = -500;
	    new_lambda = lambda;
	    
	    for(unsigned neighb_a : neighbs_a)
	    {
		if(neighbs_b.find(neighb_a) == neighbs_b.end())
		    continue;

		/*
		if(first)
		{
		    first = false;

		    const Point& neighb(all_pts[neighb_a]);
		    
		    const Vector_2 vect_neighb(neighb.x(), neighb.y()),
			t_neighb(vect_neighb - vect_t);

		    lambda = st.dot(t_neighb);
		}
		*/
		std::cout << lvl(neighb_a) << std::endl;
		
		ad_hoc_get_candidate(a, b, t, e, neighb_a, curr_e, lambda, mu,
				     new_lambda, lvl_e, vect_t, vect_s, st);
	    }
	}
	
	if(curr_e == e && go_up)
	{
	    go_up = false;
	    
	    for(unsigned neighb_a : neighbs_a)
	    {
		if(neighbs_b.find(neighb_a) == neighbs_b.end())
		    continue;

		ad_hoc_get_candidate(a, b, t, e, neighb_a, curr_e, lambda, mu,
				     new_lambda, lvl_e, vect_t, vect_s, st);
	    }
	}

	if(curr_e == e)
	{
	    bool first(true);
	    
	    for(unsigned neighb_a : neighbs_a)
	    {
		if(neighbs_b.find(neighb_a) == neighbs_b.end())
		    continue;

		/*
		if(first)
		{
		    first = false;

		    const Point& neighb(all_pts[neighb_a]);
		    
		    const Vector_2 vect_neighb(neighb.x(), neighb.y()),
			t_neighb(vect_neighb - vect_t);

		    lambda = st.dot(t_neighb);
		}
		*/
		ad_hoc_get_candidate(a, b, t, neighb_a, curr_e, lambda, mu,
				     new_lambda, vect_t, vect_s, st);
	    }
	    
	    if(curr_e == e)
		return;

	    const Triangle trg(a, b, all_pts[curr_e.b]);

	    if(in_conflict(Triangulation(), trg, t))
	    {
		std::cout << "Conflict: " << lvl(a) << " ; " << lvl(b)
			  << " ; " << lvl(curr_e.b) << std::endl;
	    }
	    
	    not_faces.push_back(trg);
	    e = curr_e;
	    continue;
	}
	
	const Triangle trg(a, b, all_pts[curr_e.b]);

	if(in_conflict(Triangulation(), trg, t))
	{
	    std::cout << "Conflict: " << lvl(a) << " ; " << lvl(b)
		      << " ; " << lvl(curr_e.b) << std::endl;
	}

	faces.push_back(trg);
	e = curr_e;
    }
}

template<class T>
void Kirkpatrick_hierarchy<T>::ad_hoc_init(
    const Point &s, const Point &t,
    const Vector_2 &vect_s, const Vector_2 &vect_t,
    const Vector_2 &st, Edge &e, std::vector<Triangle> &faces)
{
    const Lvl_info& info_s(lvls[s]);

    unsigned i, lvl_s(info_s.lvl), size;

    Edge best_e;

    Expr best_lambda, curr_lambda;

    const Node& n_s_0(get_node(s, 0));

    size = n_s_0.neighb.size();
    
    for(i = 0; i < size; ++i)
    {
	unsigned j(i + 1 < size ? i + 1 : 0);

	const Point &curr(all_pts[(n_s_0.neighb)[i]]),
	    &next(all_pts[(n_s_0.neighb)[j]]);

	if(intersects_st(vect_s, - st, curr, next, best_lambda))
	{
	    best_e.a = pts_ind[curr];
	    best_e.b = pts_ind[next];

	    intersects_st(vect_t, st, curr, next, best_lambda);
	    
	    break;
	}
    }

    /*
    for(i = 1; i <= lvl_s; ++i)
    {
	const Node& n_s(get_node(s, i));

	size = n_s.neighb.size();
	
	for(unsigned j(0); j < size; ++j)
	{
	    unsigned k(j + 1 < size ? j + 1 : 0);

	    const Point &curr(all_pts[(n_s.neighb)[j]]),
		&next(all_pts[(n_s.neighb)[k]]);

	    if(intersects_st(vect_t, st, curr, next, curr_lambda)
	       && intersects_st(vect_s, - st, curr, next))
	    {
		const Triangle trg(s, curr, next);
		
		if(curr_lambda > best_lambda &&
		   !in_conflict(Triangulation(), trg, t))
		{
		    best_e.a = pts_ind[curr];
		    best_e.b = pts_ind[next];
		}
	    }
	}
    }
    */
    e = best_e;
    
    const Triangle trg(s, all_pts[best_e.a], all_pts[best_e.b]);

    faces.push_back(trg);
}

template<class T>
bool Kirkpatrick_hierarchy<T>::ad_hoc_is_trivial(
    const Point &s, const Point &t, 
    std::vector<Triangle> &faces)
{
    const Neighbors& neighbs_s(neighborhood[s]);

    if(neighbs_s.find(pts_ind[t]) != neighbs_s.end())
    {
	const Lvl_info &info_s(lvls[s]),
	    &info_t(lvls[t]);
	    
	unsigned lvl(info_s.lvl < info_t.lvl ? info_s.lvl : info_t.lvl);

	const Node &n_s(get_node(s, lvl));

	unsigned i, j;

	is_neighbor(n_s, pts_ind[t], i);

	j = i + 1 < n_s.neighb.size() ? i + 1 : 0;

	const Triangle trg(s, t, all_pts[n_s.neighb[j]]);

	faces.push_back(trg);

	return true;
    }

    return false;
}

template<class T>
bool Kirkpatrick_hierarchy<T>::ad_hoc_is_adjacent(
    const Point &t, const Edge &e, 
    std::vector<Triangle> &faces)
{
    const Point &a(all_pts[e.a]),
	&b(all_pts[e.b]);

    if(ad_hoc_is_trivial(a, t, faces))
	return true;

    if(ad_hoc_is_trivial(b, t, faces))
	return true;

    return false;
}

template<class T>
unsigned Kirkpatrick_hierarchy<T>::lvl(unsigned p)
{
    const Point& pt(all_pts[p]);

    return lvl(pt);
}

template<class T>
unsigned Kirkpatrick_hierarchy<T>::lvl(const Point& p)
{
    const Lvl_info& info_p(lvls[p]);

    return info_p.lvl;
}

template<class T>
unsigned Kirkpatrick_hierarchy<T>::min(unsigned a, unsigned b)
{
    return a < b ? a : b;
}

template<class T>
void Kirkpatrick_hierarchy<T>::ad_hoc_find_new_face_up(
    const Point& a, const Point& b,
    Edge& e, Expr& lambda, unsigned lvl_e, const Point& t,
    const Vector_2& vect_t, const Vector_2& st, bool& go_up,
    std::vector<Triangle>& faces, std::vector<Triangle>& not_faces)
{
    unsigned i(0);

    const Node& n(get_node(a, lvl_e));

    unsigned d(n.neighb.size());

    /*
    std::cout << n.p << " ; " << a << " ; " << lvl_e << " ; "
	      << lvl(a) << std::endl;
    */
    is_neighbor(n, pts_ind[b], i);

    unsigned left(i > 0 ? i - 1 : d - 1),
	right(i + 1 < d ? i + 1 : 0);

    const Point &p_left(all_pts[n.neighb[left]]),
	&p_right(all_pts[n.neighb[right]]);

    Expr lambda_l, lambda_r;

    Edge cand_l, cand_r;
    
    ad_hoc_up_get_candidate(lambda_l, cand_l, a, b, p_left, e, vect_t, st);
    ad_hoc_up_get_candidate(lambda_r, cand_r, a, b, p_right, e, vect_t, st);
    
    Triangle trg;
    
    if(lambda_l < lambda_r)
    {
	trg = Triangle(a, b, p_left);

	if(pts_ind[p_left] == ind_a || pts_ind[p_left] == ind_b
	    || pts_ind[p_left] == ind_c)
	{
	    go_up = false;
	    return;
	}
	
	if(lambda_l <= half)
	{
	    not_faces.push_back(trg);
	    go_up = false;
	}	
	
	e = cand_l;
	lambda = lambda_l;
    }
    else
    {
	trg = Triangle(a, b, p_right);

	if(pts_ind[p_right] == ind_a || pts_ind[p_right] == ind_b
	    || pts_ind[p_right] == ind_c)
	{
	    go_up = false;
	    return;
	}
		
	if(lambda_r <= half)
	{
	    not_faces.push_back(trg);
	    go_up = false;
	}

	e = cand_r;
	lambda = lambda_r;
    }
}

template<class T>
void Kirkpatrick_hierarchy<T>::ad_hoc_get_candidate(
    const Point &a, const Point &b, const Point &t, const Edge &e,
    unsigned neighb, Edge &curr_e, Expr &lambda,
    Expr &mu, Expr &new_lambda, unsigned int lvl_e, 
    const Vector_2 &vect_t, const Vector_2& vect_s, const Vector_2 &st)
{
    Expr curr_lambda, curr_mu;
    
    unsigned curr_lvl(min(lvl_e, lvl(neighb)));

    if((neighb == ind_a) || (neighb == ind_b)
       || (neighb == ind_c))
	return;
    
    const Node &n_a(get_node(a, curr_lvl)),
	&n_neighb(get_node(neighb, curr_lvl));

    unsigned i, j, k;

    if(!is_neighbor(n_a, e.b, i) || !is_neighbor(n_neighb, e.a, j)
       || !is_neighbor(n_neighb, e.b, k))
	return;

    Triangle trg(a, b, all_pts[neighb]);

    if(in_conflict(Triangulation(), trg, t))
	return;
    
    if(ad_hoc_intersects_st(
	   vect_t, vect_s, st, a, all_pts[neighb], curr_lambda, curr_mu))
    {
	if(curr_lambda < lambda && curr_mu <= expr_one && 
	   (curr_mu > mu || (curr_mu == mu && curr_lambda < new_lambda)))
	{
	    curr_e.a = e.a;
	    curr_e.b = neighb;
	    mu = curr_mu;
	    new_lambda = curr_lambda;
	}
    }
    else if(ad_hoc_intersects_st(
		vect_t, vect_s, st, b, all_pts[neighb], curr_lambda, curr_mu))
    {
	if(curr_lambda < lambda && curr_mu <= expr_one && 
	   (curr_mu > mu || (curr_mu == mu && curr_lambda < new_lambda)))
	{
	    curr_e.a = e.b;
	    curr_e.b = neighb;
	    mu = curr_mu;
	    new_lambda = curr_lambda;
	}
    }
}

template<class T>
void Kirkpatrick_hierarchy<T>::ad_hoc_get_candidate(
    const Point &a, const Point &b, const Point &t,
    unsigned neighb, Edge &curr_e, Expr &lambda, Expr &mu, Expr &new_lambda,
    const Vector_2 &vect_t, const Vector_2& vect_s, const Vector_2 &st)
{
    Expr curr_lambda, curr_mu;

    if((neighb == ind_a) || (neighb == ind_b)
       || (neighb == ind_c))
	return;

    unsigned j;

    Triangle trg(a, b, all_pts[neighb]);

    if(in_conflict(Triangulation(), trg, t))
	return;
    
    if(ad_hoc_intersects_st(vect_t, vect_s, st, a,
			    all_pts[neighb], curr_lambda, curr_mu))
    {
	if(curr_lambda < lambda && curr_mu <= expr_one && 
	   (curr_mu > mu || (curr_mu == mu && curr_lambda < new_lambda)))
	{
	    curr_e.a = pts_ind[a];
	    curr_e.b = neighb;
	    mu = curr_mu;
	    new_lambda = curr_lambda;
	}
    }
    else if(ad_hoc_intersects_st(vect_t, vect_s, st, b,
				 all_pts[neighb], curr_lambda, curr_mu))
    {
	if(curr_lambda < lambda && curr_mu <= expr_one && 
	    (curr_mu > mu || (curr_mu == mu && curr_lambda < new_lambda)))
	{
	    curr_e.a = pts_ind[b];
	    curr_e.b = neighb;
	    mu = curr_mu;
	    new_lambda = curr_lambda;
	}
    }
}

template<class T>
void Kirkpatrick_hierarchy<T>::ad_hoc_up_get_candidate(
    Expr &lambda, Edge &cand, const Point &a, const Point &b,
    const Point &p, const Edge &e, const Vector_2 &vect_t, const Vector_2 &st)
{
    if(intersects_st(vect_t, st, a, p, lambda))
    {
	cand.a = e.a;
	cand.b = pts_ind[p];
    }
    else if(intersects_st(vect_t, st, b, p, lambda))
    {
	cand.b = pts_ind[p];
	cand.a = e.b;
    }
    else
	lambda = 500;
}

template<class T>
bool Kirkpatrick_hierarchy<T>::ad_hoc_intersects_st(
    const Vector_2 &t, const Vector_2& s,
    const Vector_2 &st, const Point &A, const Point &B,
    Expr &lambda, Expr &mu) 
{
    const Vector_2 new_a(A.x(), A.y()),
	new_b(B.x(), B.y()),
	new_t(t - new_b),
	c(new_a - new_b);

    Matrix_22 m;

    m << c, st;

    Vector_2 solution(m.fullPivLu().solve(new_t));

    const Vector_2 sa(new_a - s),
	sb(new_b - s);

    const Expr dot_a(st.dot(sa)),
	dot_b(st.dot(sb));

    std::cout << lvl(A) << " - " << lvl(B) << " : "
	      << dot_a << " - " << dot_b << std::endl;
    
    lambda = solution(1);
    mu = dot_a > dot_b ? dot_a : dot_b;
    mu /= st.squaredNorm();
    
    return ((m * solution).isApprox(new_t) && solution(0) >= 0
	    && solution(0) <= 1 && solution(1) >= 0);
}

template<class T>
void Kirkpatrick_hierarchy<T>::ad_hoc_find_new_face_down(
    const Point &a, const Point &b, Edge &e, Expr &lambda,
    const Point &t, const Vector_2 &vect_t, const Vector_2 &st,
    std::vector<Triangle> &faces, std::vector<Triangle> &not_faces)
{
    unsigned i(0), lvl_e(lvl(e));

    Node n(get_node(a, lvl_e - 1));

    unsigned d(n.neighb.size());

    if(!is_neighbor(n, pts_ind[b], i))
    {
	n = get_node(a, lvl_e);
	is_neighbor(n, pts_ind[b], i);
	d = n.neighb.size();
    }

    unsigned left(i > 0 ? i - 1 : d - 1),
	right(i + 1 < d ? i + 1 : 0);

    const Point &p_left(all_pts[n.neighb[left]]),
	&p_right(all_pts[n.neighb[right]]);

    Expr lambda_l, lambda_r;

    Edge cand_l, cand_r;
    
    ad_hoc_up_get_candidate(lambda_l, cand_l, a, b, p_left, e, vect_t, st);
    ad_hoc_up_get_candidate(lambda_r, cand_r, a, b, p_right, e, vect_t, st);
    
    Triangle trg;
    
    if(lambda_l < lambda_r)
    {
	trg = Triangle(a, b, p_left);
	
	e = cand_l;
	lambda = lambda_l;
    }
    else
    {
	trg = Triangle(a, b, p_right);

	e = cand_r;
	lambda = lambda_r;
    }    
}

template<class T>
void Kirkpatrick_hierarchy<T>::ad_hoc_routing_recursive(
    const Point &s, const Point &t, std::vector<Triangle> &faces,
    std::vector<Triangle> &not_faces)
{
    std::cout << "Ad hoc:" << std::endl;
    
    const Vector_2 vect_t(t.x(), t.y()),
	vect_s(s.x(), s.y()),
	st(vect_t - vect_s),
	perp_st(- st(1), st(0));

    Line l_st(s, t);
    
    Edge e;

    Expr lambda, norm_st(st.squaredNorm());

    bool go_up(true);
    
    if(ad_hoc_is_trivial(s, t, faces))
	return;

    ad_hoc_init(s, t, vect_s, vect_t, st, e, faces);

    intersects_st(vect_t, st, all_pts[e.a], all_pts[e.b], lambda);

    unsigned i(0);

    std::cout << ind_a << " ; " << ind_b << " ; " << ind_c
	      << std::endl;

    go_up = lambda > half;
    
    while(!ad_hoc_is_adjacent(t, e, faces) && i++ < 100)
    {
	const Point &a(all_pts[e.a]),
	    &b(all_pts[e.b]);

	intersects_st(vect_t, st, a, b, lambda);
	
	std::cout << lvl(a) << " ; " << lvl(b) << std::endl;
	
	const Neighbors &neighbs_a(neighborhood[a]),
	    &neighbs_b(neighborhood[b]);

	const unsigned lvl_e(lvl(e));

	Edge curr_e(e);
	
	if(go_up)
	    ad_hoc_rec_find_new_face_up(a, b, curr_e, lambda, lvl_e, t,
					vect_s, vect_t, st, perp_st, l_st,
					go_up, faces, not_faces);

	if(!go_up)
	{
	    Point new_s(furthest_along_st(a, b, vect_s, st));

	    Expr curr_dist(dist_along_st(new_s, vect_s, st));

	    std::cout << curr_dist / norm_st << std::endl;
	    
	    if(curr_dist < third * norm_st)
	    {
		std::cout << "Need to change new source" << std::endl;
		
		const Neighbors& neighbs_new_s(neighborhood[new_s]);

		for(unsigned neighb : neighbs_new_s)
		{
		    Expr neighb_dist(
			dist_along_st(all_pts[neighb], vect_s, st));

		    if(neighb_dist <= norm_st && neighb_dist > curr_dist)
		    {
			std::cout << "Changed new source" << std::endl;
			new_s = all_pts[neighb];
			curr_dist = neighb_dist;
		    }
		}

		faces.push_back(induced_triangle(curr_e, new_s));
		
		std::cout << (curr_dist < third * norm_st) << std::endl;
	    }

	    std::cout << "End level: " << lvl_e << std::endl;
	    ad_hoc_routing_recursive(new_s, t, faces, not_faces);
	    return;
	}
	
	const Triangle trg(a, b, all_pts[curr_e.b]);

	if(in_conflict(Triangulation(), trg, t))
	{
	    std::cout << "Conflict: " << lvl(a) << " ; " << lvl(b)
		      << " ; " << lvl(curr_e.b) << std::endl;
	}

	faces.push_back(trg);
	e = curr_e;
    }

}

template<class T>
void Kirkpatrick_hierarchy<T>::ad_hoc_rec_find_new_face_up(
    const Point& a, const Point& b,
    Edge& e, Expr& lambda, unsigned lvl_e, const Point& t,
    const Vector_2& vect_s, const Vector_2& vect_t, const Vector_2& st,
    const Vector_2& perp_st, const Line& l_st, bool& go_up, 
    std::vector<Triangle>& faces, std::vector<Triangle>& not_faces)
{
    unsigned i(0);

    const Node& n(get_node(a, lvl_e));

    unsigned d(n.neighb.size());

    /*
    std::cout << n.p << " ; " << a << " ; " << lvl_e << " ; "
	      << lvl(a) << std::endl;
    */
    is_neighbor(n, pts_ind[b], i);

    unsigned left(i > 0 ? i - 1 : d - 1),
	right(i + 1 < d ? i + 1 : 0);

    const Point &p_left(all_pts[n.neighb[left]]),
	&p_right(all_pts[n.neighb[right]]);

    Expr lambda_l, lambda_r;

    Edge cand_l, cand_r;
    
    ad_hoc_up_get_candidate(lambda_l, cand_l, a, b, p_left, e, vect_t, st);
    ad_hoc_up_get_candidate(lambda_r, cand_r, a, b, p_right, e, vect_t, st);
    
    Triangle trg;
    
    if(lambda_l < lambda_r && lambda_l < lambda)
    {
	trg = Triangle(a, b, p_left);

	if(pts_ind[p_left] == ind_a || pts_ind[p_left] == ind_b
	    || pts_ind[p_left] == ind_c)
	{
	    std::cout << "Left to cross: " << lambda << std::endl;
	    go_up = false;
	    return;
	}

	if(in_conflict(Triangulation(), trg, t))
	    //|| is_risky(a, b, p_left, t, l_st, perp_st, vect_s, st))
	{
	    std::cout << "Left to cross: " << lambda << std::endl;
	    go_up = false;
	    return;
	}
	
	e = cand_l;
	lambda = lambda_l;
    }
    else if(lambda_r < lambda)
    {
	trg = Triangle(a, b, p_right);

	if(pts_ind[p_right] == ind_a || pts_ind[p_right] == ind_b
	    || pts_ind[p_right] == ind_c)
	{
	    std::cout << "Left to cross: " << lambda << std::endl;
	    go_up = false;
	    return;
	}
		
	if(in_conflict(Triangulation(), trg, t))
	    //|| is_risky(a, b, p_right, t, l_st, perp_st, vect_s, st))
	{
	    std::cout << "Left to cross: " << lambda << std::endl;
	    go_up = false;
	    return;
	}

	e = cand_r;
	lambda = lambda_r;
    }
    else
    {
	std::cout << "Left to cross: " << lambda << std::endl;
	go_up = false;
    }
}

template<class T>
typename Kirkpatrick_hierarchy<T>::Point
Kirkpatrick_hierarchy<T>::furthest_along_st(
    const Point &A, const Point &B, const Vector_2 &s, const Vector_2 &st)
{
    const Expr dot_a(dist_along_st(A, s, st)),
	dot_b(dist_along_st(B, s, st));

    if(dot_a > dot_b)
	return A;
    else
	return B;
}

template<class T>
typename Kirkpatrick_hierarchy<T>::Expr
Kirkpatrick_hierarchy<T>::dist_along_st(
    const Point &p, const Vector_2 &s, const Vector_2 &st)
{
    const Vector_2 new_p(p.x(), p.y()),
	sp(new_p - s);

    const Expr dot_p(st.dot(sp));

    return dot_p;
}

template<class T>
typename Kirkpatrick_hierarchy<T>::Triangle
Kirkpatrick_hierarchy<T>::induced_triangle(const Edge &e, const Point &p)
{
    const Point &q(all_pts[e.a]),
	&r(all_pts[e.b]);

    return Triangle(p, q, r);
}

template<class T>
void Kirkpatrick_hierarchy<T>::routing_path_backtrack(
    const Point &s, const Point &t, std::vector<Triangle> &faces,
    std::vector<Triangle> &not_faces)
{
    if(ad_hoc_is_trivial(s, t, faces))
	return;

    const Vector_2 vect_s(s.x(), s.y()),
	vect_t(t.x(), t.y()),
	ts(vect_s - vect_t);

    std::stack<Edge> edges;

    Edge e_s;
    
    bool go_up(true);

    ad_hoc_init(s, t, vect_s, vect_t, - ts, e_s, faces);

    edges.push(e_s);
    
    unsigned i(0), curr_lvl(lvl(e_s)); 

    while(!ad_hoc_is_adjacent(t, e_s, faces) && i++ < 50)
    {
	std::cout << "In routing_path" << std::endl;
    
	routing_get_new_face_backtrack(
	    curr_lvl, e_s, t, vect_s, ts, go_up, edges, faces,
	    not_faces);

	edges.push(e_s);
    }
}

template<class T>
void Kirkpatrick_hierarchy<T>::ad_hoc_routing_pov(
    const Point &s, const Point &t, std::vector<Triangle> &faces,
    std::vector<Triangle> &not_faces)
{
    std::cout << "Ad hoc:" << std::endl;
    
    const Vector_2 vect_t(t.x(), t.y()),
	vect_s(s.x(), s.y()),
	st(vect_t - vect_s),
	perp_st(- st(1), st(0));

    Line l_st(s, t);
    
    Edge e;

    Expr lambda, norm_st(st.squaredNorm());

    bool go_up(true), could_have_been_at_b(true);

    Point old(s), next_old;

    if(ad_hoc_is_trivial(s, t, faces))
	return;

    ad_hoc_init(s, t, vect_s, vect_t, st, e, faces);

    intersects_st(vect_t, st, all_pts[e.a], all_pts[e.b], lambda);

    unsigned i(0);

    std::cout << ind_a << " ; " << ind_b << " ; " << ind_c
	      << std::endl;

    while(!ad_hoc_is_adjacent(t, e, faces) && i++ < 30)
    {
	const Point &a(all_pts[e.a]),
	    &b(all_pts[e.b]);

	intersects_st(vect_t, st, a, b, lambda);
	
	std::cout << lvl(a) << " ; " << lvl(b) << " : "
		  << ad_hoc_is_adjacent(t, e, faces) << std::endl;
	
	const Neighbors &neighbs_a(neighborhood[a]),
	    &neighbs_b(neighborhood[b]);

	const unsigned lvl_e(lvl(e));

	Edge curr_e(e);

	Expr old_lambda(lambda);
	
	Triangle new_face;

	if(go_up)
	{
	    ad_hoc_pov_find_new_face_up(a, b, curr_e, lambda, lvl_e, t,
					vect_s, vect_t, st, perp_st, l_st,
					go_up, faces, not_faces);

	    new_face = Triangle(a, b, all_pts[curr_e.b]);

	    if(curr_e.a == e.a)
		next_old = b;
	    else
		next_old = a;
	}
      

	if(!go_up)
	{
	    std::cout << "lvl_old: " << lvl(old) << std::endl;
	    
	    //Circle old_circle(a, b, old);
	    
	    unsigned candidate(e.a), lvl_cand(lvl(candidate)), lvl_neighb;

	    Edge candidate_e(e.a, e.b), neighb_e;

	    bool risky(true), not_delaunay(true);

	    std::cout << lvl(a) << " : ";
	    
	    for(unsigned neighb : neighbs_a)
	    {
		if(neighb == pts_ind[b] || neighb == ind_a
		   || neighb == ind_b || neighb == ind_c
		   || lvl(neighb) > lvl(e))
		    continue;
		
		const Point &p(all_pts[neighb]);

		Expr dist_p;

		bool new_risky;

		lvl_neighb = lvl(neighb);

		new_risky = is_risky(a, b, p, t, l_st, perp_st, vect_s, st);
		
		if(!intersects_st(vect_t, st, a, p, dist_p))
		{
		    if(!intersects_st(vect_t, st, b, p, dist_p))
			continue;

		    //new_risky = is_risky(old_circle, b, p);

		    neighb_e.a = e.b;
		    neighb_e.b = candidate;
		}
		else
		{
		    //new_risky = is_risky(old_circle, a, p);

		    neighb_e.a = e.a;
		    neighb_e.b = candidate;
		}

		Triangle trg(a, b, p);

		if(dist_p < old_lambda
		   && dist_along_st(p, vect_s, st) <= norm_st
		   && !in_conflict(Triangulation(), trg, t))
		{		    
		    bool new_not_delaunay(in_conflict(
					      Triangulation(), trg, old));
		    
		    std::cout << lvl(neighb) << " ( "
			      << (new_risky || new_not_delaunay) << " ) - ";
		    
		    if(not_delaunay)
		    {
			if(!new_not_delaunay)
			{
			    lambda = dist_p;
			    candidate = neighb;
			    risky = new_risky;
			    not_delaunay = false;
			    candidate_e = neighb_e;
			    lvl_cand = lvl_neighb;
			}
			else if(dist_p < lambda)
			{
			    lambda = dist_p;
			    candidate = neighb;
			    candidate_e = neighb_e;
			    lvl_cand = lvl_neighb;
			}
		    }
		    else if(risky && !new_not_delaunay &&
			    (!new_risky ||
			     /*(candidate_e.a == e.b &&
			       (neighb_e.a == e.a || */
			     (lvl_cand < lvl_neighb)))//))
		    {
			lambda = dist_p;
			candidate = neighb;
			risky = new_risky;
			candidate_e = neighb_e;
			lvl_cand = lvl_neighb;
		    }
		    else if(!risky && !new_not_delaunay &&
			    !new_risky && lvl_cand < lvl_neighb)
		    {
			lambda = dist_p;
			candidate = neighb;
			candidate_e = neighb_e;
			lvl_cand = lvl_neighb;
		    }
		}
		else
		    std::cout << lvl(neighb) << " ( skipped ) - "; 
	    }

	    std::cout << std::endl << "is risky: " << risky << std::endl;

	    bool no_cand(candidate == e.a);

	    if(no_cand)
	    {
		std::cout << "No candidate found" << std::endl;

		for(unsigned neighb : neighbs_a)
		{
		    if(neighb == pts_ind[b] || neighb == ind_a
		       || neighb == ind_b || neighb == ind_c
		       || lvl(neighb) <= lvl(e))
			continue;
		
		    const Point &p(all_pts[neighb]);

		    Expr dist_p;

		    bool new_risky;

		    lvl_neighb = lvl(neighb);

		    new_risky = is_risky(a, b, p, t, l_st, perp_st, vect_s, st);
		
		    if(!intersects_st(vect_t, st, a, p, dist_p))
		    {
			if(!intersects_st(vect_t, st, b, p, dist_p))
			    continue;

			//new_risky = is_risky(old_circle, b, p);

			neighb_e.a = e.b;
			neighb_e.b = candidate;
		    }
		    else
		    {
			//new_risky = is_risky(old_circle, a, p);

			neighb_e.a = e.a;
			neighb_e.b = candidate;
		    }

		    Triangle trg(a, b, p);

		    if(dist_p < old_lambda
		       && dist_along_st(p, vect_s, st) <= norm_st
		       && !in_conflict(Triangulation(), trg, t))
		    {
			std::cout << lvl(neighb) << " ( "  << new_risky << " ) - ";
		    
			bool new_not_delaunay(in_conflict(
						  Triangulation(), trg, old));

			if(not_delaunay)
			{
			    if(!new_not_delaunay)
			    {
				lambda = dist_p;
				candidate = neighb;
				risky = new_risky;
				not_delaunay = false;
				candidate_e = neighb_e;
				lvl_cand = lvl_neighb;
			    }
			    else if(dist_p < lambda)
			    {
				lambda = dist_p;
				candidate = neighb;
				candidate_e = neighb_e;
				lvl_cand = lvl_neighb;
			    }
			}
			else if(risky && !new_not_delaunay &&
				(!new_risky ||
				 /*(candidate_e.a == e.b &&
				   (neighb_e.a == e.a || */
				 (lvl_cand < lvl_neighb)))//))
			{
			    lambda = dist_p;
			    candidate = neighb;
			    risky = new_risky;
			    candidate_e = neighb_e;
			    lvl_cand = lvl_neighb;
			}
			else if(!risky && !new_not_delaunay &&
				!new_risky && lvl_cand < lvl_neighb)
			{
			    lambda = dist_p;
			    candidate = neighb;
			    candidate_e = neighb_e;
			    lvl_cand = lvl_neighb;
			}
		    }
		    else
			std::cout << lvl(neighb) << " ( skipped ) - "; 
		}
		
		no_cand = candidate == e.a;
	    }
	    
	    if(no_cand)
	    {
		std::cout << "No candidate from a" << std::endl;

		for(unsigned neighb : neighbs_b)
		{
		    if(neighb == pts_ind[b] || neighb == ind_a
		       || neighb == ind_b || neighb == ind_c)
			continue;

		    const Point &p(all_pts[neighb]);

		    Expr dist_p;

		    bool new_risky;

		    new_risky = is_risky(a, b, p, t, l_st, perp_st, vect_s, st);
		    
		    if(!intersects_st(vect_t, st, a, p, dist_p))
		    {
			if(!intersects_st(vect_t, st, b, p, dist_p))
			    continue;

			//new_risky = is_risky(old_circle, b, p);

			neighb_e.a = e.b;
			neighb_e.b = candidate;
		    }
		    else
		    {
			//new_risky = is_risky(old_circle, a, p);

			neighb_e.a = e.a;
			neighb_e.b = candidate;
		    }

		    Triangle trg(a, b, p);

		    if(dist_p < old_lambda
		       && dist_along_st(p, vect_s, st) <= norm_st
		       && !in_conflict(Triangulation(), trg, t))
		    {
			std::cout << lvl(neighb) << " ( "
				  << new_risky << " ) - ";
					
			bool new_not_delaunay(in_conflict(
						  Triangulation(), trg, old));

			if(not_delaunay)
			{
			    if(!new_not_delaunay)
			    {
				lambda = dist_p;
				candidate = neighb;
				risky = new_risky;
				not_delaunay = false;
				candidate_e = neighb_e;
			    }
			    else if(dist_p < lambda)
			    {
				lambda = dist_p;
				candidate = neighb;
				candidate_e = neighb_e;
			    }
			}
			else if(risky && !new_not_delaunay &&
				(!new_risky ||
				 (candidate_e.a == e.b && (neighb_e.a == e.a ||
							   (dist_p < lambda)))))
			{
			    lambda = dist_p;
			    candidate = neighb;
			    risky = new_risky;
			    candidate_e = neighb_e;
			}
		    }
		}
	    }
	    
	    if(intersects_st(vect_t, st, a, all_pts[candidate]))
	    {
		curr_e.b = candidate;
		next_old = b;
		
		std::cout << "new edge exists: "
			  << (neighbs_a.find(candidate) != neighbs_a.end())
			  << std::endl;
	    }
	    else
	    {
		if(neighbs_a.find(e.b) != neighbs_a.end() && !could_have_been_at_b)
		{
		    curr_e.a = e.b;
		    curr_e.b = candidate;
		}
		else
		    curr_e.a = candidate;

		next_old = a;
		
		std::cout << "new edge exists: "
			  << (neighbs_b.find(candidate) != neighbs_b.end())
			  << std::endl;
	    }

	    new_face = Triangle(a, b, all_pts[candidate]);
	}
		    
	if(in_conflict(Triangulation(), new_face, t))
	{
	    std::cout << "Conflict with t: " << lvl(new_face[0])
		      << " ; " << lvl(new_face[1])
		      << " ; " << lvl(new_face[2]) << std::endl;
	}

	std::cout << lvl(old) << std::endl;
	
	if(go_up)
	    faces.push_back(new_face);
	else
	{
	    if(in_conflict(Triangulation(), new_face, old))
	    {
		std::cout << "Conflict with c: " << lvl(new_face[0])
			  << " ; " << lvl(new_face[1])
			  << " ; " << lvl(new_face[2]) << std::endl;

		not_faces.push_back(new_face);
		
		if(!intersects_st(vect_s, st, new_face[0], new_face[2]))
		{
		    if(!intersects_st(vect_s, st, old, new_face[2]))
		    {
			std::cout << "no intersection" << std::endl;
			curr_e.a = pts_ind[new_face[2]];
			curr_e.b = e.b;
			next_old = old;
		    }
		    else
		    {
			std::cout << "old intersects" << std::endl;
			curr_e.a = pts_ind[new_face[2]];
			curr_e.b = pts_ind[old];
			next_old = a;
		    }
		}
		else
		{
		    std::cout << "a intersects" << std::endl;
		    curr_e.a = e.a;
		    curr_e.b = pts_ind[new_face[2]];
		    next_old = old;
		}
		
		new_face = Triangle(a, new_face[2], old);
	    }
	    
	    not_faces.push_back(new_face);
	}

	old = next_old;
	could_have_been_at_b = (curr_e.a != e.a || curr_e.b == e.b);
	e = curr_e;
    }
}

template<class T>
typename Kirkpatrick_hierarchy<T>::Expr
Kirkpatrick_hierarchy<T>::max(Expr e1, Expr e2)
{
    return e1 > e2 ? e1 : e2;
}

template<class T>
typename Kirkpatrick_hierarchy<T>::Point
Kirkpatrick_hierarchy<T>::furthest_along_st(
    const Point &A, const Point &B, const Point &C,
    const Vector_2 &s, const Vector_2 &st)
{
    const Point f_ab(furthest_along_st(A, B, s, st)),
	f_abc(furthest_along_st(f_ab, C, s, st));

    return f_abc;
}

template<class T>
bool Kirkpatrick_hierarchy<T>::is_risky(
    const Point &a, const Point &b, const Point &p,
    const Point& t, const Line& st, const Vector_2& perp_st,
    const Vector_2& vect_s, const Vector_2& vect_st)
{
    const Circle circ_abp(a, b, p);

    const Point& C(circ_abp.center());

    // halfway could also be the middle point, needs further testing
    
    const Point furthest(furthest_along_st(a, b, p, vect_s, vect_st)),
	proj(st.projection(furthest)),
	halfway((2 * proj.x() + t.x()) / 3 - C.x(),
		(2 * proj.y() + t.y()) / 3 - C.y()),
	p_on_perp(halfway.x() + perp_st(0), halfway.y() + perp_st(1));

    const Expr &sq_r(circ_abp.squared_radius()),
	norm_perp_st(perp_st.squaredNorm()),
	D(halfway.x() * p_on_perp.y() - halfway.y() * p_on_perp.x()),
	discr(sq_r * norm_perp_st - D * D);

    return discr >= 0;
}

template<class T>
bool Kirkpatrick_hierarchy<T>::is_risky(
    const Circle &old_circle, const Point &new_a, const Point &new_b)
{
    const Point& C(old_circle.center());

    const Vector_2 ab(new_b.x() - new_a.x(), new_b.y() - new_a.y());

    const Expr norm_ab(ab.squaredNorm()),
	D(ab(0) * (new_a.x() - C.x()) +
	  ab(1) * (new_a.y() - C.y()));
    
    return (D < 0) && (- 2 * D <= norm_ab);
}

template<class T>
bool Kirkpatrick_hierarchy<T>::intersect(
    const Edge& e1, const Edge& e2)
{
    if(e1.a == e2.a)
	return false;

    const Point &a(all_pts[e1.a]),
	&b(all_pts[e1.b]),
	&c(all_pts[e2.a]),
	&d(all_pts[e2.b]);

    const Vector_2 vect_a(a.x(), a.y()),
	vect_b(b.x(), b.y()),
	vect_c(c.x(), c.y()),
	vect_d(d.x(), d.y()),
	ba(vect_a - vect_b),
	cd(vect_d - vect_c),
	bd(vect_d - vect_b);

    Matrix_22 m;

    m << ba, cd;

    Vector_2 solution(m.fullPivLu().solve(bd));

    return ((m * solution).isApprox(bd) && solution(0) > 0 &&
	    solution(0) < 1 && solution(1) > 0 && solution(1) < 1);
}

template<class T>
void Kirkpatrick_hierarchy<T>::ad_hoc_pov_find_new_face_up(
    const Point& a, const Point& b,
    Edge& e, Expr& lambda, unsigned lvl_e, const Point& t,
    const Vector_2& vect_s, const Vector_2& vect_t, const Vector_2& st,
    const Vector_2& perp_st, const Line& l_st, bool& go_up,
    std::vector<Triangle>& faces, std::vector<Triangle>& not_faces)
{
    unsigned i(0);

    const Node& n(get_node(a, lvl_e));

    unsigned d(n.neighb.size());

    /*
    std::cout << n.p << " ; " << a << " ; " << lvl_e << " ; "
	      << lvl(a) << std::endl;
    */
    is_neighbor(n, pts_ind[b], i);

    unsigned left(i > 0 ? i - 1 : d - 1),
	right(i + 1 < d ? i + 1 : 0);

    const Point &p_left(all_pts[n.neighb[left]]),
	&p_right(all_pts[n.neighb[right]]);

    Expr lambda_l, lambda_r;

    Edge cand_l, cand_r;
    
    ad_hoc_up_get_candidate(lambda_l, cand_l, a, b, p_left, e, vect_t, st);
    ad_hoc_up_get_candidate(lambda_r, cand_r, a, b, p_right, e, vect_t, st);
    
    Triangle trg;
    
    if(lambda_l < lambda_r && lambda_l < lambda)
    {
	trg = Triangle(a, b, p_left);

	if(pts_ind[p_left] == ind_a || pts_ind[p_left] == ind_b
	    || pts_ind[p_left] == ind_c)
	{
	    go_up = false;
	    return;
	}

	if(in_conflict(Triangulation(), trg, t) ||
	   is_risky(a, b, p_left, t, l_st, perp_st, vect_s, st))
	{
	    go_up = false;
	    return;
	}

	//go_up = lambda_l > half;
	e = cand_l;
	lambda = lambda_l;
    }
    else if(lambda_r < lambda)
    {
	trg = Triangle(a, b, p_right);

	if(pts_ind[p_right] == ind_a || pts_ind[p_right] == ind_b
	    || pts_ind[p_right] == ind_c)
	{
	    go_up = false;
	    return;
	}
		
	if(in_conflict(Triangulation(), trg, t) ||
	    is_risky(a, b, p_right, t, l_st, perp_st, vect_s, st))
	{
	    go_up = false;
	    return;
	}

	//go_up = lambda_r > half;	
	e = cand_r;
	lambda = lambda_r;
    }
    else
	go_up = false;
}

template<class T>
unsigned Kirkpatrick_hierarchy<T>::max_lvl(const Edge& e)
{
    unsigned lvl_a(lvl(e.a)), lvl_b(lvl(e.b));

    return lvl_a < lvl_b ? lvl_b : lvl_a;
}

#endif

